/**
 * 
 */

// Contact controller
		webProject.controller('contactController',function($scope, $location){
			
			var map;
			var latlng=new google.maps.LatLng( 42.004325, 21.409516);
	
			var myOptions = {
			    zoom: 15,
			    center: latlng,
			    mapTypeId: 'terrain'
			};
			
			map = new google.maps.Map($('#map')[0], myOptions);
			
			var marker = new google.maps.Marker({
			    position: latlng,
			    map: map,
			  	title:'Student\'s office Location'
			
			});
			
				
		});
		