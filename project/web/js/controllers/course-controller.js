/**
 * 
 */

//----------------------------------------------------------// Courses controller -----------------------------------------------------------
		webProject.controller('coursesController',function($scope,  $http){
			
			$('textarea').css('overflow', 'hidden').autogrow();
			$scope.courseOrderBy='courseName';
			$scope.showAll=true;
			$scope.showOne=false;
			
			$scope.orderByMeCourse = function(x) {
				
		        $scope.courseOrderBy = x;
		    }
			$scope.backFunction=function(){
				$scope.showOne=false;
				$scope.showAll=true;
				$scope.updateMessageData="";
			}
			$scope.getAllCourses=function(){
				$http({
	                method: 'GET',
	                url: getServiceUrl()+'/course/all',               
	            }).then(function(response){
	            	//alert('response: ' + response.data);
	            	
	            	$scope.allCourses = response.data;
	            	
	            	
	            	
	            	
	            },function(xhr){
	            
	            	alert("error getting students");
	            	$scope.allCourses = [];
	            });
				
			}
			$scope.courseDetails=function(id){
				$scope.showOne=true;
				$scope.showAll=false;
			  $http({
	                method: 'GET',
	                url: getServiceUrl()+'/course/find/'+id,               
	            }).then(function(response){
	            	// alert('response: ' + response.data);
	            	$scope.courseData = response.data;
	            	
	            	
	            	// Course data
	            	$scope.courseId=$scope.courseData.courseId;
	            	$scope.name=$scope.courseData.courseName;
	            	$scope.classes=$scope.courseData.fund_classes;
	            	$scope.program_content=$scope.courseData.contentOfProgram;
	            	$scope.language=$scope.courseData.language;
	            	$scope.professor=$scope.courseData.professor; 
	            	$scope.program_goal=$scope.courseData.goalOfProgram; 
	            	
	            },function(){
	            	alert("error getting courses");
	            	$scope.courseData = [];
	            });
			
			}
			$scope.deleteCourse=function(id){
				
				$http({
	                method: 'DELETE',
	                url: getServiceUrl()+'/course/delete/'+id,               
	            }).then(function(response){
	            	//alert('response: ' + response.data);
	            	if(response.data.status=="err"){
	        			$scope.actionResponse=response.data.message;
	        			return;
	        		}
	            	
	            
	            	$scope.getAllCourses();
	            },function(){
	            	alert("error deleting course.");
	            });
					
			}
			$scope.loadCourseData=function(){
				$scope.addCourseData={};
				
				$scope.addCourseData.courseName=$scope.add_name;
				$scope.addCourseData.fund_classes=$scope.add_classes;
				$scope.addCourseData.contentOfProgram=$scope.add_program_content;
				$scope.addCourseData.language=$scope.add_language;
				$scope.addCourseData.professor=$scope.add_professor;
				$scope.addCourseData.goalOfProgram=$scope.add_program_goal;
			}
			
			$scope.clearAddCourseModels=function(){
				$scope.add_name="";
				$scope.add_classes="";
				$scope.add_program_content="";
				$scope.add_language="";
				$scope.add_professor="";
				$scope.add_program_goal="";
				
			}
			$scope.addCourse=function(){
				
				$scope.loadCourseData();
				
				
				 $http({
		                method: 'POST',
		                url: getServiceUrl()+'/course/addCourse',     
		                data: $scope.addCourseData
		            }).then(function(response){
		            //	alert('response: ' + response.data);
		            	if(response.data.status=="err"){
		            			$scope.addCourseData=response.data.message;
		            			return;
		            		}
		            	
		            	$scope.clearAddCourseModels();
		            	
		            	
		            	$scope.addCourseMessage=response.data.message;
		            	
		            	
		            	$scope.getAllCourses();
		            	
		            },function(err){
		            	alert("error adding course.");
		            	$scope.addCourseMessage="Cannot add course";
		            });
				
				
			}
			$scope.loadUpdateCourseData=function(){
				$scope.updateCourseData={};
				
				$scope.updateCourseData.courseId=$scope.courseId;
				$scope.updateCourseData.courseName=$scope.name;
				$scope.updateCourseData.fund_classes=$scope.classes;
				$scope.updateCourseData.contentOfProgram=$scope.program_content;
				$scope.updateCourseData.goalOfProgram=$scope.program_goal;
				$scope.updateCourseData.language=$scope.language;
				$scope.updateCourseData.professor=$scope.professor;
				
				
			}
			$scope.updateCourse=function(){
					
				$scope.loadUpdateCourseData();
				
				 $http({
		                method: 'POST',
		                url: getServiceUrl()+'/course/updateCourse',     
		                data: $scope.updateCourseData
		            }).then(function(response){
		            //	alert('response: ' + response.data);
		            	if(response.data.status=="err"){
		            			$scope.updateMessageData=response.data.message;
		            			return;
		            		}
		                   	
		            	
		            	$scope.updateMessageData=response.data.message;
		            	
		            	
		            	$scope.getAllCourses();
		            	
		            },function(err){
		            	alert("error updating course.");
		            	$scope.updateMessageData="Cannot update course";
		            });
			}
			
			//$scope.deleteCourse(3);
			$scope.getAllCourses();
		});
		