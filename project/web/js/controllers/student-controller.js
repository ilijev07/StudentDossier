/**
 * 
 */

	// Students controller
		webProject.controller('studentsController',function($scope, $http, $modal){
			
			
			
			
			
			$("#accordion").show().accordion({
		        active: false,
		        heightStyle: "content",
	            autoHeight: false,
	            clearStyle: true,   
		        navigation: true,
		        collapsible: true
		    });
			
			$("#accordion1").show().accordion({
		        active: false,
		        heightStyle: "content",
	            autoHeight: false,
	            clearStyle: true,   
		        navigation: true,
		        collapsible: true
		    });
			
			$scope.myOrderBy='index';
			$scope.courseOrderBy='courseName';
			$scope.showAll=true;
			$scope.showOne=false;
			$scope.getAllStudents = function(){
				 $http({
		                method: 'GET',
		                url: getServiceUrl()+'/student/all',               
		            }).then(function(response){
		            //	alert('response: ' + response.data);
		            	$scope.allStudents = response.data;
		            	
		            },function(xhr){
		            	alert("error getting students");
		            	$scope.allStudents = [];
		            });
			}
			
		
			$scope.orderByMe = function(x) {
				
			        $scope.myOrderBy = x;
			    }
			$scope.orderByMeCourse = function(x) {
				
		        $scope.courseOrderBy = x;
		    }
			$scope.getAllStudentCourses=function(id){
				 var gradeSix=0,gradeSeven=0,gradeEight=0,gradeNine=0,gradeTen=0;
				 $http({
		                method: 'GET',
		                url: getServiceUrl()+'/course/allStudentCourses/'+id,               
		            }).then(function(response){
		            	// alert('response: ' + response.data);
		            	$scope.allStudentCourses = response.data;
		            	
						 
						 for(var i=0;i < $scope.allStudentCourses.length; i++)
							 {
							 	if($scope.allStudentCourses[i].grade=='6')
							 		gradeSix++;
							 	else if($scope.allStudentCourses[i].grade=='7')
							 		gradeSeven++;
							 	else if($scope.allStudentCourses[i].grade=='8')
							 		gradeEight++;
							 	else if($scope.allStudentCourses[i].grade=='9')
							 		gradeNine++;
							 	else if($scope.allStudentCourses[i].grade=='10')
							 		gradeTen++;
							 	var donutChart;
							 	  donutChart = c3.generate({
									     bindto: '#donut-grades',
									     data: {
									         columns: [
									             ['Grade 6', gradeSix],
									             ['Grade 7',  gradeSeven],
									             ['Grade 8', gradeEight],
									             ['Grade 9', gradeNine],
									             ['Grade 10', gradeTen]
									         ],
									         type : 'donut',
									         onclick: function (d, i) { console.log("onclick", d, i); },
									         onmouseover: function (d, i) { console.log("onmouseover", d, i); },
									         onmouseout: function (d, i) { console.log("onmouseout", d, i); }
									     },
									     donut: {
									         title: "Grades chart"
									     }
									 });
							 	
							 }
		            	
		            },function(){
		            	alert("error getting student");
		            	$scope.allStudentCourses = [];
		            });
				 
				
				 
				
			}
			$scope.studentDetails=function(id){
					$scope.showOne=true;
					$scope.showAll=false;
				  $http({
		                method: 'GET',
		                url: getServiceUrl()+'/student/find/'+id,               
		            }).then(function(response){
		            	// alert('response: ' + response.data);
		            	$scope.studentData = response.data;
		            	
		            	
		            	// Personal data
		            	$scope.id=$scope.studentData.id;
		            	$scope.name=$scope.studentData.name;
		            	$scope.surname=$scope.studentData.surname;
		            	$scope.embg=$scope.studentData.embg;
		            	
		            	$scope.date=new Date($scope.studentData.dateOfBirth);
		            	$scope.nationality=$scope.studentData.nationality;
		            	$scope.gender=$scope.studentData.gender; 
		            	$scope.citizenship=$scope.studentData.citizenship; 
		            	$scope.place_of_birth=$scope.studentData.placeOfBirth;
		            	$scope.municipality_of_birth=$scope.studentData.municipalityOfBirth; 
		            	$scope.country_of_birth=$scope.studentData.countryOfBirth;
		            	
		            	// faculty data
		            	$scope.index=$scope.studentData.index;
		            	$scope.status=$scope.studentData.status;
		            	$scope.cycle=$scope.studentData.cycle;
		            	$scope.quota=$scope.studentData.quota;
		            	$scope.studies_program=$scope.studentData.studiesProgram;
		            	$scope.year_enrollment=$scope.studentData.yearOfEnrollment;
	
		            	//Contact data
		          
		            		$scope.place_living=$scope.studentData.placeOfLiving;
		            		$scope.municipality_living=$scope.studentData.municipalityOfLiving;
		            		$scope.country=$scope.studentData.country;
		            		$scope.address=$scope.studentData.address;
		            		$scope.phone=$scope.studentData.phone;
		            		$scope.email=$scope.studentData.email;
		           
		            		
		            		/// PROBA
		            		
		            		$scope.getAllStudentCourses($scope.id);
		            		
		            		
		            		
		            	
		            },function(){
		            	alert("error getting student");
		            	$scope.studentData = [];
		            });
				
			}
			$scope.deleteStudentCourse=function(courseId){
				
				$http({
	                method: 'DELETE',
	                url: getServiceUrl()+'/student/deleteSC/'+$scope.id+'/'+courseId,               
	            }).then(function(response){
	            	//alert('response: ' + response.data);
	            	if(response.data.status=="err"){
	            			response.data.message;
	        			return;
	        		}
	            	//alert(response.data.message);
	            
	            	$scope.studentDetails($scope.id);
	            	
	            	$scope.getAllStudentCourses($scope.id);
	            },function(){
	            	alert("error deleting course .");
	            });
			}
			$scope.backFunction=function(){
				$scope.showOne=false;
				$scope.showAll=true;
			}
			
			$scope.getAllCourses=function(){
				$http({
	                method: 'GET',
	                url: getServiceUrl()+'/course/all',               
	            }).then(function(response){
	            	//alert('response: ' + response.data);
	            	
	            	$scope.allCourses = response.data;
	            	
	            	if(angular.equals([], $scope.allCourses))
	            		
	            		$scope.messageCourses="This student does not have data for courses";
	            	else
	            		$scope.messageCourses="Courses successfuly loaded";
	            	
	            	
	            },function(xhr){
	            	$scope.messageCourses="Cannot get courses data";
	            	alert("error getting students");
	            	$scope.allCourses = [];
	            });
				
			}
			$scope.addStudent=function(){
				
				
				$scope.loadStudentData();
				
				
				 $http({
		                method: 'POST',
		                url: getServiceUrl()+'/student/addStudent',     
		                data: $scope.addStudentData
		            }).then(function(response){
		            //	alert('response: ' + response.data);
		            	if(response.data.status=="err"){
		            			$scope.addStudentMessage=response.data.message;
		            			return;
		            		}
		            	
		            	$scope.clearAddStudentModels();
		            	
		            	
		            	$scope.addStudentMessage=response.data.message;
		            	
		            	
		            	$scope.getAllStudents();
		            	
		            },function(err){
		            	alert("error adding student.");
		            });
				
				
			}
			
			$scope.clearAddStudentModels=function(){
				//Faculty data
				$scope.add_index="";
				$scope.add_year_enrollment="";
				$scope.add_status="";
				$scope.add_cycle="";
				$scope.add_quota="";
				$scope.add_studies_program="";
				
				// Contact data
				$scope.add_place_living="";
				$scope.add_municipality_living="";
				$scope.add_country="";
				$scope.add_address="";
				$scope.add_phone="";
				
				// Email
				$scope.add_email="";
				
				
				// Personal info
				$scope.add_name="";
				$scope.add_surname="";
				$scope.add_embg="";
				$scope.add_date="";
				$scope.add_nationality="";
				$scope.add_gender="";
            	$scope.add_citizenship="";
            	$scope.add_place_of_birth="";
            	$scope.add_municipality_of_birth="";
            	$scope.add_country_of_birth="";
            	$scope.addStudentMessage="";
			}
			
			$scope.loadStudentData=function(){
				
				$scope.addStudentData={};
				
				//Faculty data
				$scope.addStudentData.index=$scope.add_index;
				$scope.addStudentData.yearOfEnrollment=$scope.add_year_enrollment;
				$scope.addStudentData.status=$scope.add_status;
				$scope.addStudentData.cycle=$scope.add_cycle;
				$scope.addStudentData.quota=$scope.add_quota;
				$scope.addStudentData.studiesProgram=$scope.add_studies_program;
				
				// Contact data
				$scope.addStudentData.placeOfLiving=$scope.add_place_living;
				$scope.addStudentData.municipalityOfLiving=$scope.add_municipality_living
				$scope.addStudentData.country=$scope.add_country;
				$scope.addStudentData.address=$scope.add_address;
				$scope.addStudentData.phone=$scope.add_phone;
				
				// Email
				$scope.addStudentData.email=$scope.add_email;
				
				
				// Personal info
				$scope.addStudentData.name=$scope.add_name;
				$scope.addStudentData.surname=$scope.add_surname;
				$scope.addStudentData.embg=$scope.add_embg;
				$scope.addStudentData.dateOfBirth=$scope.add_date;
				$scope.addStudentData.nationality=$scope.add_nationality;
				$scope.addStudentData.gender=$scope.add_gender;
            	$scope.addStudentData.citizenship=$scope.add_citizenship;
            	$scope.addStudentData.placeOfBirth=$scope.add_place_of_birth;
            	$scope.addStudentData.municipalityOfBirth=$scope.add_municipality_of_birth;
            	$scope.addStudentData.countryOfBirth=$scope.add_country_of_birth;
            	
            	$scope.addStudentData.studentCourse={};
			}
			
			
			$scope.deleteStudent=function(id){
				
				$http({
	                method: 'DELETE',
	                url: getServiceUrl()+'/student/delete/'+id,               
	            }).then(function(response){
	            	//alert('response: ' + response.data);
	            	if(response.data.status=="err"){
	        			$scope.actionResponse=response.data.message;
	        			return;
	        		}
	            	
	            
	            	$scope.getAllStudents();
	            },function(){
	            	alert("error deleting student.");
	            });
				
				
			}
			
		
			$scope.loadUpdateStudentData=function(){
				
				$scope.updateStudentData={};
				// Personal data
				$scope.updateStudentData.id=$scope.id;
				$scope.updateStudentData.name=$scope.name;
				$scope.updateStudentData.surname=$scope.surname;
				$scope.updateStudentData.embg=$scope.embg;
            	
				$scope.updateStudentData.dateOfBirth=$scope.date;
				$scope.updateStudentData.nationality=$scope.nationality;
				$scope.updateStudentData.gender=$scope.gender;
				$scope.updateStudentData.citizenship=$scope.citizenship;
				$scope.updateStudentData.placeOfBirth=$scope.place_of_birth;
				$scope.updateStudentData.municipalityOfBirth=$scope.municipality_of_birth; 
				$scope.updateStudentData.countryOfBirth=$scope.country_of_birth;
            	
            	// faculty data
				$scope.updateStudentData.index=$scope.index;
				$scope.updateStudentData.status=$scope.status;
				$scope.updateStudentData.cycle=$scope.cycle;
				$scope.updateStudentData.quota=$scope.quota;
				$scope.updateStudentData.studiesProgram=$scope.studies_program;
				$scope.updateStudentData.yearOfEnrollment=$scope.year_enrollment;

            	//Contact data
          
				$scope.updateStudentData.placeOfLiving=$scope.place_living;
				$scope.updateStudentData.municipalityOfLiving=$scope.municipality_living;
				$scope.updateStudentData.country=$scope.country;
				$scope.updateStudentData.address=$scope.address;
				$scope.updateStudentData.phone=$scope.phone;
				$scope.updateStudentData.email=$scope.email;
				
			}
			
			$scope.updateStudent=function(){
					
					
					$scope.loadUpdateStudentData();
					
					
					 $http({
			                method: 'POST',
			                url: getServiceUrl()+'/student/updateStudent',     
			                data: $scope.updateStudentData
			            }).then(function(response){
			            //	alert('response: ' + response.data);
			            	if(response.data.status=="err"){
			            			$scope.updateMessageData=response.data.message;
			            			return;
			            		}
			            	
			            	$scope.updateMessageData=response.data.message;
			            	
			            	$scope.getAllStudents();
			            	
			            	
			            },function(err){
			            	alert("error updating student.");
			            });
					
					
			}
			$scope.openModalDialog=function(courseId){
				
				
				$http({
	                method: 'GET',
	                url: getServiceUrl()+'/course/find/'+courseId,               
	            }).then(function(response){
	            	// alert('response: ' + response.data);
	            	$scope.courseData = response.data;      
	            	
	            	
	            },function(){
	            	alert("error getting courses");
	            	$scope.courseData = [];
	            });
				
			
				$modal.open({
					templateUrl:'modal.html',
					controller: 'modalController', 
					scope: $scope
					
				});
			}
			
		
			$scope.getAllStudents();
			
			$scope.getAllCourses();
			
				
		});