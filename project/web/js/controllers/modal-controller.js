/**
 * 
 */

//------------------------------------------// Modal controller ------------------------------------------------------------------
		webProject.controller('modalController',function($scope, $http, $modalInstance){
		
		
			$scope.submitForm=function(){
				
				$scope.addStudentCourse={};
				
				$scope.addStudentCourse.course=$scope.courseData;
				$scope.addStudentCourse.student=$scope.studentData;
				$scope.addStudentCourse.grade=$scope.grade;
				$scope.addStudentCourse.schoolYear=$scope.year;
				$scope.addStudentCourse.semester=$scope.semester;
				
				
				 $http({
		                method: 'POST',
		                url: getServiceUrl()+'/course/addStudentCourse',     
		                data: $scope.addStudentCourse
		            }).then(function(response){
		            //	alert('response: ' + response.data);

		            	$scope.addStudentCourseMsg=response.data.message;
		            	$scope.getAllStudentCourses($scope.addStudentCourse.student.id);
		            },function(err){
		            	alert("error adding student course data .");
		            
		            });
				
				
			}
			
			$scope.cancelForm=function(){
				
				$modalInstance.dismiss();
			}
			
				
		});

