
		var webProject = angular.module('webProject', ['ngRoute', 'ngMessages','ui.bootstrap']);
		
		// configuring routes 
		
		webProject.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
			 $routeProvider
		      .when('/', {
		        templateUrl: 'home.html', 
		        controller: 'mainController'
		      })
		      .when('/students', {
		        templateUrl: 'students.html',
		        controller: 'studentsController'
		      })
		      .when('/courses', {
			        templateUrl: 'courses.html',
			        controller: 'coursesController'
			  }).when('/contact', {
			        templateUrl: 'contact.html',
			        controller: 'contactController'
			  }); 
		}]); 
		
		
		
		
		// Main controller
		webProject.controller('mainController', function($scope) {
			//alert("main controller");
	        $scope.message = 'Welcome to Student\'s office of FINKI';
	        
	    });
		
		// Header controller 
		webProject.controller('headerController',function($scope, $location){
				
			//alert("header controller");
			$scope.isActive=function(viewLocation){
				
				return viewLocation == $location.path();
			};		
		});
		
	

		
	
		
		
		
		
		