package com.web.app.project.service;

import java.util.List;

import javax.annotation.Resource;


import org.springframework.stereotype.Component;

import com.web.app.project.dao.IStudentDao;


import com.web.app.project.model.Student;


@Component
public class StudentServiceImpl implements IStudentService{

	
	
	IStudentDao studentDao;
	
	
	@Resource(name="studentDaoImpl")
	@Override
	public void setStudentDao(IStudentDao studentDao)
	{
		this.studentDao=studentDao;
	}
	@Override
	public Student findById(long id){
		
		return studentDao.findById(id);
	}

	@Override
	public List<Student> getAllStudents() {
		
		return studentDao.getAllStudents();
	}

	@Override
	public Object addStudent(Student student) {
		
		return studentDao.addStudent(student);
	}

	@Override
	public Object deleteStudent(long id) {
		
		return studentDao.deleteStudent(id);
	}


	@Override
	public Object updateStudent(Student student) {
		
		return studentDao.updateStudent(student);
	}

	@Override
	public Object deleteStudentCourse(long id, long course_id) {
		return studentDao.deleteStudentCourse(id, course_id);
	}

	
}
