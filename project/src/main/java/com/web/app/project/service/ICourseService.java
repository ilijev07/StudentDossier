package com.web.app.project.service;

import java.util.List;



import com.web.app.project.dao.ICourseDao;

import com.web.app.project.model.Course;


import com.web.app.project.model.StudentCourse;

public interface ICourseService {
	
	
	public void setCourseDao(ICourseDao courseDao);
	
	public Course findById(long id);
	
	public Object addCourse(Course course);
	
	public Object deleteCourse(long course_id);
	
	public List<Course> getAllCourses();
	
	public Object updateCourse(Course course);
	
	public Object addStudentData(StudentCourse studentCourse);
	
	public List<StudentCourse> getAllStudentCourses(long id);

}
