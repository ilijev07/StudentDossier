package com.web.app.project.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.web.app.project.model.Course;
import com.web.app.project.model.Student;

import com.web.app.project.service.IStudentService;

@RestController
@RequestMapping(value="/student")
public class StudentController {
	
	@Resource(name="studentServiceImpl")
	IStudentService studentService;
	
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<Student> findById(@PathVariable("id") String id)
	{
		Student student=studentService.findById(Long.parseLong(id));
		if(student==null){
			return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
		}
		else 
			return new ResponseEntity<Student>(student,HttpStatus.OK);
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET, produces="application/json")
	public List<Student> getAllStudents()
	{
		return studentService.getAllStudents();
	}
	

	
	@RequestMapping(value="/addStudent", method=RequestMethod.POST, produces="application/json")
	public Object addStudent(@RequestBody Student addStudentData)
	{
		return studentService.addStudent(addStudentData);
	}
	
	@RequestMapping(value="/updateStudent", method=RequestMethod.POST, produces="application/json")
	public Object updateStudent(@RequestBody Student updateStudentData)
	{
		return studentService.updateStudent(updateStudentData);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE, produces="application/json")
	public Object deleteStudent(@PathVariable("id") long id)
	{
		return studentService.deleteStudent(id);
	}
	
	@RequestMapping(value="/deleteSC/{id}/{course_id}", method=RequestMethod.DELETE, produces="application/json")
	public Object deleteStudentCourse(@PathVariable("id") long id, @PathVariable("course_id") long course_id)
	{
		return studentService.deleteStudentCourse(id,course_id);
	}
	

}
