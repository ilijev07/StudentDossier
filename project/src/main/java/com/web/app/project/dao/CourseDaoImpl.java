package com.web.app.project.dao;

import java.util.*;

import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.web.app.project.model.Course;

import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;
import javax.persistence.NoResultException;



@Repository
public class CourseDaoImpl implements ICourseDao{

	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public Course findById(long course_id) {
		Course course=null;
		try{
			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Course> cq = cb.createQuery(Course.class);
	        Root<Course> r = cq.from(Course.class);
	        ParameterExpression<Long> p = cb.parameter(Long.class);
	        cq.select(r).where(cb.equal(r.get("courseId"), p));
	        TypedQuery<Course> query = em.createQuery(cq);
	        query.setParameter(p, course_id);
	        
	        course = query.getSingleResult();
		}
		catch(NoResultException noResultException)
		{
			course=null;
			System.out.println("Exception caught");
		}		
                
		
		return course;
	}

	@Override
	@Transactional
	public Object addCourse(Course course) {
		
		HashMap<String,String> result = new HashMap<String,String> ();
		if(checkCourse(course))
		{
			em.persist(course);
			
			
			result.put("status", "ok");
			result.put("message","Course successfully added");
		}
		else 
		{
			result.put("status", "err");
			result.put("message","Course is not successfully added");
		}
		
		return result;
		
	}

	@Override
	@Transactional
	public Object deleteCourse(long course_id) {
		
		HashMap<String,String> result = new HashMap<String,String> ();
		
		try{

			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Course> cq = cb.createQuery(Course.class);
	        Root<Course> r = cq.from(Course.class);
	        ParameterExpression<Long> p = cb.parameter(Long.class);
	        cq.select(r).where(cb.equal(r.get("courseId"), p));
	        TypedQuery<Course> query = em.createQuery(cq);
	        query.setParameter(p, course_id);
	        Course course = query.getSingleResult();
	        
	        
	        CriteriaBuilder cb2 = em.getCriteriaBuilder();
	        CriteriaDelete<StudentCourse> cd2 = cb2.createCriteriaDelete(StudentCourse.class);
	        Root<StudentCourse> r2 = cd2.from(StudentCourse.class);
	        cd2.where(cb2.equal(r2.get("course"), course));
	        em.createQuery(cd2).executeUpdate();
	        

			  CriteriaBuilder cb1 = em.getCriteriaBuilder();
		      CriteriaDelete<Course> cd1 = cb1.createCriteriaDelete(Course.class);
		      Root<Course> r1 = cd1.from(Course.class);
		      cd1.where(cb1.equal(r1.get("courseId"), course_id));
		      em.createQuery(cd1).executeUpdate();
		
			
			result.put("status", "ok");
			result.put("message","Course successfully deleted");
			
		}
		catch(NoResultException noResultException){	
			
			  result.put("status", "err");
		      result.put("message","Course is not successfully deleted");
			
		}
		
		
		return result;
	}
	
	
	
	@Override
	public List<Course> getAllCourses() {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Course> cq = cb.createQuery(Course.class);
        Root<Course> r = cq.from(Course.class);
        cq.select(r);
        TypedQuery<Course> query = em.createQuery(cq);
       
        List<Course> results = query.getResultList();
        
 		return results;
	
	}


	


	@Override
	@Transactional
	public Object updateCourse(Course course) {
		
		HashMap<String,String> result = new HashMap<String,String> ();
        	
		if(checkCourse(course)){
			 CriteriaBuilder cb = this.em.getCriteriaBuilder();
	           
	          // create update
	          CriteriaUpdate<Course> update = cb.
	             createCriteriaUpdate(Course.class);
	           
	          // set the root class
	          Root<Course> e = update.from(Course.class);
	           
	          // set update and where clause
	          update.set("courseName", course.getCourseName());
	          update.set("fund_classes", course.getFund_classes());
	          update.set("contentOfProgram", course.getContentOfProgram());
	          update.set("goalOfProgram", course.getGoalOfProgram());
	          update.set("language", course.getLanguage());
	          update.set("professor", course.getProfessor());
	          update.where(cb.equal(e.get("courseId"), course.getCourseId()));
	           
	          // perform update
	          this.em.createQuery(update).executeUpdate();
	         
	         
	  
	 
	        result.put("status", "ok");
	        result.put("message","Course successfully updated");
		}
		else
		{
			result.put("status", "err");
	        result.put("message","Course is not successfully updated");
		}
        
         
        return result;
	}

	@Override
	@Transactional
	public Object addStudentData(StudentCourse studentCourse) {
		
		
		
		HashMap<String,String> result = new HashMap<String,String> ();
		
		if(checkStudentCourse(studentCourse))
		{
			em.persist(studentCourse);

			result.put("status", "ok");
			result.put("message","Student course data successfully added");
		}
		else{
			
			result.put("status", "err");
			result.put("message","Student course data is not successfully added");
		}
		
		
		return result;
	}

	@Override
	public List<StudentCourse> getAllStudentCourses(long id) {
		  List<StudentCourse> results=null;
		try{
			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
	        Root<Student> r = cq.from(Student.class);
	        ParameterExpression<Long> p = cb.parameter(Long.class);
	        cq.select(r).where(cb.equal(r.get("id"), p));
	        TypedQuery<Student> query = em.createQuery(cq);
	        query.setParameter(p, id);
	        Student student = query.getSingleResult();
	        
	        
	       results=student.getStudentCourse();
			
		}
		catch(NoResultException noResultException)
		{
			results=null;
			System.out.println("No entity found for query");
		}
		
        
        
		return results;
	}
	
	
	public boolean checkCourse(Course course){
		
		if(course.getCourseName()==null || course.getContentOfProgram()==null || course.getFund_classes()==null
				|| course.getGoalOfProgram()==null || course.getLanguage()==null || course.getProfessor()==null)
			return false;
		else if(course.getCourseName()=="" || course.getContentOfProgram()=="" || course.getFund_classes()==""
				|| course.getGoalOfProgram()=="" || course.getLanguage()=="" || course.getProfessor()=="")
			return false;
		
		return true;
	}
	
	public boolean checkStudentCourse(StudentCourse studentCourse)
	{
		if(studentCourse.getCourse()==null || studentCourse.getStudent()==null || studentCourse.getGrade()<5.0 || studentCourse.getGrade()>10.0
				|| studentCourse.getSchoolYear()<0 || studentCourse.getSemester()==null || studentCourse.getSemester()=="")
			return false;
		
		return true;
	}
}
