package com.web.app.project.dao;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.stereotype.Repository;


import com.web.app.project.model.Course;

import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;



@Repository
public class StudentDaoImpl implements IStudentDao{
	
		
	@PersistenceContext
    private EntityManager em;
	
	
	@Override
	public Student findById(long id) throws NoResultException{
		 Student student =null;
		try{
			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
	        Root<Student> r = cq.from(Student.class);
	        ParameterExpression<Long> p = cb.parameter(Long.class);
	        cq.select(r).where(cb.equal(r.get("id"), p));
	        TypedQuery<Student> query = em.createQuery(cq);
	        query.setParameter(p, id);
	        student = query.getSingleResult();
			
	       // System.out.println(student.toString());
		} 
		catch(NoResultException noResultException)
		{
			
			student=null;
			
		}
		
        return student;
	}

	
	@Override
	public List<Student> getAllStudents() {
		  CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
	        Root<Student> r = cq.from(Student.class);
	        cq.select(r);
	        TypedQuery<Student> query = em.createQuery(cq);
	       
	        List<Student> results = query.getResultList();
	   
	       
 		return results;
	}
	// org.springframework.dao.DataIntegrityViolationException
	@Override
	@Transactional
	public Object addStudent(Student student) {
		HashMap<String,String> result = new HashMap<String,String> ();
		if(checkStudent(student))
		{
			em.persist(student);
			result.put("status", "ok");
			result.put("message","Student successfully added");
		}
			
			
		else {

			result.put("status", "err");
			result.put("message","Student is not added successfully");
		}
		
		
		return result;
		
	}

	@Override
	@Transactional
	public Object deleteStudent(long id) throws NoResultException{
		 Student student=null;
		 HashMap<String,String> result = new HashMap<String,String> ();
		try{
			
			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
	        Root<Student> r = cq.from(Student.class);
	        ParameterExpression<Long> p = cb.parameter(Long.class);
	        cq.select(r).where(cb.equal(r.get("id"), p));
	        TypedQuery<Student> query = em.createQuery(cq);
	        query.setParameter(p, id);
	       student  = query.getSingleResult();
	       
	       
	       CriteriaBuilder cb2 = em.getCriteriaBuilder();
	        CriteriaDelete<StudentCourse> cd2 = cb2.createCriteriaDelete(StudentCourse.class);
	        Root<StudentCourse> r2 = cd2.from(StudentCourse.class);
	        cd2.where(cb2.equal(r2.get("student"), student));
	        em.createQuery(cd2).executeUpdate();
	        
	        
			
			  CriteriaBuilder cb1 = em.getCriteriaBuilder();
		      CriteriaDelete<Student> cd1 = cb1.createCriteriaDelete(Student.class);
		      Root<Student> r1 = cd1.from(Student.class);
		      cd1.where(cb1.equal(r1.get("id"), id));
		      em.createQuery(cd1).executeUpdate();
		      
		      result.put("status", "ok");
		      result.put("message","Student successfully deleted");
		      
		} catch (NoResultException noResException)
		{
			 student=null;

			result.put("status", "err");
			result.put("message","Student is not successfully deleted");
		}
		
		return result;
		
	}
	
	@Override
	@Transactional
	public Object updateStudent(Student student) {
		
		HashMap<String,String> result = new HashMap<String,String> ();
         
         
         CriteriaBuilder cb = this.em.getCriteriaBuilder();
       
        	  // create update
              CriteriaUpdate<Student> update = cb.
                 createCriteriaUpdate(Student.class);
               
              // set the root class
              Root<Student> e = update.from(Student.class);
               if(checkStudent(student)){
            	   // set update and where clause
                   update.set("index", student.getIndex());
                   update.set("yearOfEnrollment", student.getYearOfEnrollment());
                   update.set("status", student.getStatus());
                   update.set("cycle", student.getCycle());
                   update.set("quota", student.getQuota());
                   update.set("studiesProgram", student.getStudiesProgram());
                    
                   update.set("placeOfLiving", student.getPlaceOfLiving());
                   update.set("municipalityOfLiving", student.getMunicipalityOfLiving());
                   update.set("country", student.getCountry());
                   update.set("address", student.getAddress());
                   update.set("phone", student.getPhone());
                   update.set("email", student.getEmail());
                   update.set("name", student.getName());
                   update.set("surname", student.getSurname());
                   update.set("EMBG", student.getEMBG());
                   update.set("dateOfBirth", student.getDateOfBirth());  
                   update.set("nationality", student.getNationality());
                   update.set("gender", student.getGender());
                   update.set("citizenship", student.getCitizenship());
                   update.set("placeOfBirth", student.getPlaceOfBirth());
                    
                   update.set("municipalityOfBirth", student.getMunicipalityOfBirth());
                   update.set("countryOfBirth", student.getCountryOfBirth());
                    
                   update.where(cb.equal(e.get("id"), student.getId()));
                    
                   // perform update
                   this.em.createQuery(update).executeUpdate();
                   
                   result.put("status", "ok");
                   result.put("message","Student successfully updated");
               }
               else 
               {
            	   result.put("status", "err");
                   result.put("message","Student is not successfully updated");
               }
             
               
               
       
           
        //em.merge(student);
       
        return result;
	}


	@Override
	@Transactional
	public Object deleteStudentCourse(long id,long course_id) {
		
		HashMap<String,String> result = new HashMap<String,String> ();
		try{
			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
	        Root<Student> r = cq.from(Student.class);
	        ParameterExpression<Long> p = cb.parameter(Long.class);
	        cq.select(r).where(cb.equal(r.get("id"), p));
	        TypedQuery<Student> query = em.createQuery(cq);
	        query.setParameter(p, id);
	        Student student = query.getSingleResult();
	        
	        CriteriaBuilder cb2 = em.getCriteriaBuilder();
	        CriteriaQuery<Course> cq2 = cb2.createQuery(Course.class);
	        Root<Course> r2 = cq2.from(Course.class);
	        ParameterExpression<Long> p2 = cb2.parameter(Long.class);
	        cq2.select(r2).where(cb2.equal(r2.get("courseId"), p2));
	        TypedQuery<Course> query2 = em.createQuery(cq2);
	        query2.setParameter(p2, course_id);
	        Course course = query2.getSingleResult();
	        
	        
	        
	        CriteriaBuilder cb1 = em.getCriteriaBuilder();
	        CriteriaDelete<StudentCourse> cd1 = cb1.createCriteriaDelete(StudentCourse.class);
	        Root<StudentCourse> r1 = cd1.from(StudentCourse.class);
	        cd1.where(cb1.equal(r1.get("student"), student),cb1.equal(r1.get("course"), course));
	        em.createQuery(cd1).executeUpdate();
	        
	        result.put("status", "ok");
	        result.put("message","Student course data successfully deleted");
	        
		}
		catch(NoResultException noResultException){	
			
			  result.put("status", "err");
		      result.put("message","Student course data is not successfully deleted");
			
		}
      
        
        
		
		return result;
	}


	
	public boolean checkStudent(Student student){
		
		if(student.getName()==null || student.getSurname()==null || student.getEMBG() == null || student.getDateOfBirth() == null 
				|| student.getPlaceOfLiving()== null || student.getMunicipalityOfLiving()== null || student.getCountry()==null
				|| student.getAddress()==null || student.getPhone()==null || student.getEmail()==null)
			return false;
		
		else 
			if(student.getName()=="" || student.getSurname()=="" || student.getEMBG() == "" || student.getDateOfBirth() == null 
			|| student.getPlaceOfLiving()== "" || student.getMunicipalityOfLiving()== "" || student.getCountry()==""
			|| student.getAddress()=="" || student.getPhone()=="" || student.getEmail()=="")
		return false;
		
		return true;
	}
	

}
