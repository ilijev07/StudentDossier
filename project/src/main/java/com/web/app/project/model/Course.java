package com.web.app.project.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Ilija on 16-Aug-16.
 */

@Entity
@Table(name="COURSE")
public class Course implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="course_id")
	private long courseId;
	
	@Column(name="course_name",nullable=false)
	private String courseName;
	
	@Column(name="fund_classes",nullable=false)
	private String fund_classes;
	
	@Column(name="content_of_program",nullable=false)
	private String contentOfProgram;
	
	@Column(name="goal_of_program",nullable=false)
	private String goalOfProgram;
	
	@Column(name="language",nullable=false)
	private String language;

	@Column(name="professor",nullable=false)
	private String professor;
	
	
	@OneToMany(mappedBy="course",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<StudentCourse> studentCourse;
	
	
	public Course()
	{
		
	}
	
	

	@JsonIgnore
	public List<StudentCourse> getStudentCourse() {
		return studentCourse;
	}



	
	public void setStudentCourse(List<StudentCourse> studentCourse) {
		this.studentCourse = studentCourse;
	}




	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	

	public long getCourseId() {
		return courseId;
	}
	
	
	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getFund_classes() {
		return fund_classes;
	}

	public void setFund_classes(String fund_classes) {
		this.fund_classes = fund_classes;
	}

	public String getContentOfProgram() {
		return contentOfProgram;
	}

	public void setContentOfProgram(String contentOfProgram) {
		this.contentOfProgram = contentOfProgram;
	}

	public String getGoalOfProgram() {
		return goalOfProgram;
	}

	public void setGoalOfProgram(String goalOfProgram) {
		this.goalOfProgram = goalOfProgram;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contentOfProgram == null) ? 0 : contentOfProgram.hashCode());
		result = prime * result + (int) (courseId ^ (courseId >>> 32));
		result = prime * result + ((courseName == null) ? 0 : courseName.hashCode());
		result = prime * result + ((fund_classes == null) ? 0 : fund_classes.hashCode());
		result = prime * result + ((goalOfProgram == null) ? 0 : goalOfProgram.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		result = prime * result + ((studentCourse == null) ? 0 : studentCourse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		
		if(this.contentOfProgram.equals(other.contentOfProgram) && this.courseId==other.courseId && this.courseName.equals(other.courseName)
				&& this.fund_classes.equals(other.fund_classes) && this.goalOfProgram.equals(other.goalOfProgram)
				&& this.language.equals(other.language) && this.professor.equals(other.professor))
			return true;
		
		return false;
	}
	
	
	
	
}
