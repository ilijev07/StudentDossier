package com.web.app.project.service;

import com.web.app.project.dao.IStudentDao;


import com.web.app.project.model.Student;


import java.util.*;


public interface IStudentService {
	
	
	public void setStudentDao(IStudentDao studentDao);
	
	public Student findById(long id) ;
	
	public List<Student> getAllStudents();
	
	public Object addStudent(Student student);
	
	public Object deleteStudent(long id);
	
	
	public Object updateStudent(Student student);
	
	public Object deleteStudentCourse(long id,long course_id);
}
