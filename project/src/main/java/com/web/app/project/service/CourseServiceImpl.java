package com.web.app.project.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.web.app.project.dao.ICourseDao;
import com.web.app.project.dao.IStudentDao;
import com.web.app.project.model.Course;


import com.web.app.project.model.StudentCourse;

@Component
public class CourseServiceImpl implements ICourseService{
	
	
	ICourseDao courseDao;
	
	
	@Resource(name="courseDaoImpl")
	@Override
	public void setCourseDao(ICourseDao courseDao)
	{
		this.courseDao=courseDao;
	}
	
	
	@Override
	public Course findById(long id) {
		return courseDao.findById(id);
	}

	@Override
	public Object addCourse(Course course) {
		return courseDao.addCourse(course);
	}

	@Override
	public Object deleteCourse(long course_id) {
		return courseDao.deleteCourse(course_id);
	}


	@Override
	public List<Course> getAllCourses() {
		return courseDao.getAllCourses();
	}


	@Override
	public Object updateCourse(Course course) {
		return courseDao.updateCourse(course);
	}

	@Override
	public Object addStudentData(StudentCourse studentCourse) {
		return courseDao.addStudentData(studentCourse);
	}

	@Override
	public List<StudentCourse> getAllStudentCourses(long id) {
		return courseDao.getAllStudentCourses(id);
	}

}
