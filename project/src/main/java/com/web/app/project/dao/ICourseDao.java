package com.web.app.project.dao;

import com.web.app.project.model.Course;


import com.web.app.project.model.StudentCourse;

import java.util.*;

public interface ICourseDao {
	
	public Course findById(long course_id);
	
	public Object addCourse(Course course);
	
	public Object deleteCourse(long course_id);
	
	public List<Course> getAllCourses();
	
	
	public Object updateCourse(Course course);
	
	public Object addStudentData(StudentCourse studentCourse);
	
	public List<StudentCourse> getAllStudentCourses(long id);
	
}
