package com.web.app.project.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.web.app.project.model.Course;
import com.web.app.project.model.StudentCourse;
import com.web.app.project.service.ICourseService;

@RestController
@RequestMapping(value="/course")
public class CourseController {
	
	
	
	@Resource(name="courseServiceImpl")
	ICourseService courseService;
	
	
	
	@RequestMapping(value="/all", method=RequestMethod.GET, produces="application/json")
	public List<Course> getAllCourses()
	{
		return courseService.getAllCourses();
	}
	
	
	@RequestMapping(value="/find/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<Course> findById(@PathVariable("id") long id)
	{
		Course course=courseService.findById(id);
		if(course==null)
			return new ResponseEntity<Course>(HttpStatus.NOT_FOUND);
		
		else
			return new ResponseEntity<Course>(course,HttpStatus.OK);
	}
	
	@RequestMapping(value="/addCourse", method=RequestMethod.POST, produces="application/json")
	public Object addCourse(@RequestBody Course course)
	{
		return courseService.addCourse(course);
	}
	@RequestMapping(value="/updateCourse", method=RequestMethod.POST, produces="application/json")
	public Object updateCourse(@RequestBody Course course)
	{
		return courseService.updateCourse(course);
	}
	
	@RequestMapping(value="/addStudentCourse", method=RequestMethod.POST, produces="application/json")
	public Object addStudentCourse(@RequestBody StudentCourse studentCourse)
	{
		return courseService.addStudentData(studentCourse);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE, produces="application/json")
	public Object deleteCourse(@PathVariable("id") long id)
	{
		return courseService.deleteCourse(id);
	}
	
	@RequestMapping(value="/allStudentCourses/{id}", method=RequestMethod.GET, produces="application/json")
	public Object getAllStudentCourses(@PathVariable("id") long id)
	{
		List<StudentCourse> studentCourses=courseService.getAllStudentCourses(id);
		if(studentCourses==null)
			return new ResponseEntity<List<StudentCourse>>(HttpStatus.NOT_FOUND);
		
		else
			
			return new ResponseEntity<List<StudentCourse>>(studentCourses,HttpStatus.OK);
	}
	

}
