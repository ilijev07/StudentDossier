package com.web.app.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

/**
 * Created by Ilija on 11-Aug-16.
 */

@Entity
@Table(name="STUDENT")

public class Student implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	//Faculty data 
	
	@Column(name="ind")
    private int index; // ima 
   
	
	@Column(name="year_of_enrollment")
    private String yearOfEnrollment; // ima 
	
	
	@Column(name="stat")
	@Enumerated(EnumType.STRING)
    private Status status;  //ima
	
	@Column(name="cycle")
	@Enumerated(EnumType.STRING)
    private StudiesCycle cycle; //ima
	
	@Column(name="quota")
	@Enumerated(EnumType.STRING)
    private Quota quota;   //ima
	
	@Column(name="studies_program")
    private String studiesProgram; //ima
	
	// Contact 
	 @Column(name="place_of_living",nullable = false)
	  private String placeOfLiving; //ima
	  
	  @Column(name="municipality_of_living",nullable = false)
	  private String municipalityOfLiving; //ima
	  
	  @Column(name="country",nullable = false)
	  private String country; //ima
	  
	  @Column(name="address",nullable = false)
	  private String address;  //ima
	 
	  @Column(name="phone",nullable = false, unique= true)
	  private String phone;  //ima
	  
	  @Column(name="email",nullable = false, unique= true)
	  private String email;    
	  
	  
	  // Personal info 
	  
	 
		
		@Column(name="name",nullable=false)
	 	private String name;
		
		@Column(name="surname",nullable=false)
	    private String surname;
		
		@Column(name="EMBG",nullable=false, unique=true)
	    private String EMBG;
		
		@Column(name="date_of_birth",nullable=false)
	    private Date dateOfBirth;
		
		@Column(name="nationality")
	    private String nationality;
		
		@Column(name="gender")
		@Enumerated(EnumType.STRING)
	    private Gender gender;
		
		@Column(name="citizenship")
	    private String citizenship;
		
		@Column(name="place_of_birth")
	    private String placeOfBirth;
		
		@Column(name="municipality_of_birth")
	    private String municipalityOfBirth;
		
		@Column(name="country_of_birth")
	    private String countryOfBirth;
	
	
	@OneToMany(mappedBy="student",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<StudentCourse> studentCourse;
	
	
	
	
	
	
	public Student()
	{
		
	}
	@JsonIgnore
	public List<StudentCourse> getStudentCourse() {
		return studentCourse;
	}





	public void setStudentCourse(List<StudentCourse> studentCourse) {
		this.studentCourse = studentCourse;
	}
	
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEMBG() {
		return EMBG;
	}

	public void setEMBG(String eMBG) {
		EMBG = eMBG;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getMunicipalityOfBirth() {
		return municipalityOfBirth;
	}

	public void setMunicipalityOfBirth(String municipalityOfBirth) {
		this.municipalityOfBirth = municipalityOfBirth;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getPlaceOfLiving() {
		return placeOfLiving;
	}

	public void setPlaceOfLiving(String placeOfLiving) {
		this.placeOfLiving = placeOfLiving;
	}

	public String getMunicipalityOfLiving() {
		return municipalityOfLiving;
	}

	public void setMunicipalityOfLiving(String municipalityOfLiving) {
		this.municipalityOfLiving = municipalityOfLiving;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}


	public String getYearOfEnrollment() {
		return yearOfEnrollment;
	}


	public void setYearOfEnrollment(String yearOfEnrollment) {
		this.yearOfEnrollment = yearOfEnrollment;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public StudiesCycle getCycle() {
		return cycle;
	}


	public void setCycle(StudiesCycle cycle) {
		this.cycle = cycle;
	}


	public Quota getQuota() {
		return quota;
	}


	public void setQuota(Quota quota) {
		this.quota = quota;
	}


	public String getStudiesProgram() {
		return studiesProgram;
	}


	public void setStudiesProgram(String studiesProgram) {
		this.studiesProgram = studiesProgram;
	}




	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((EMBG == null) ? 0 : EMBG.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((citizenship == null) ? 0 : citizenship.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((countryOfBirth == null) ? 0 : countryOfBirth.hashCode());
		result = prime * result + ((cycle == null) ? 0 : cycle.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + index;
		result = prime * result + ((municipalityOfBirth == null) ? 0 : municipalityOfBirth.hashCode());
		result = prime * result + ((municipalityOfLiving == null) ? 0 : municipalityOfLiving.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nationality == null) ? 0 : nationality.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((placeOfBirth == null) ? 0 : placeOfBirth.hashCode());
		result = prime * result + ((placeOfLiving == null) ? 0 : placeOfLiving.hashCode());
		result = prime * result + ((quota == null) ? 0 : quota.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((studentCourse == null) ? 0 : studentCourse.hashCode());
		result = prime * result + ((studiesProgram == null) ? 0 : studiesProgram.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((yearOfEnrollment == null) ? 0 : yearOfEnrollment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (this.address.equals(other.address) && this.citizenship.equals(other.citizenship) && this.country.equals(other.country) 
				&& this.countryOfBirth.equals(other.countryOfBirth) && this.dateOfBirth.equals(other.dateOfBirth) && this.email.equals(other.email)
				&& this.EMBG.equals(other.EMBG) && this.gender.equals(other.gender) && this.id==other.id 
				&& this.municipalityOfBirth.equals(other.municipalityOfBirth) && this.municipalityOfLiving.equals(other.municipalityOfLiving)
				&& this.name.equals(other.name) && this.surname.equals(other.surname) && this.nationality.equals(other.nationality)
				&& this.phone.equals(other.phone)
				&& this.cycle.equals(other.cycle) && this.index==other.index && this.quota.equals(other.quota)
				&& this.status.equals(other.status) && this.studiesProgram.equals(other.studiesProgram)
				&& this.yearOfEnrollment.equals(other.yearOfEnrollment))
			return true;
			
		
		return false;
	}

	@Override
	public String toString() {
		
		return "Student with name: "+this.getName();
	}


	
	
  



}

