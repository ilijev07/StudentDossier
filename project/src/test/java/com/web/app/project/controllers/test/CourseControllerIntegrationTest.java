	package com.web.app.project.controllers.test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Date;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;
import com.web.app.project.helper.TestUtil;
import com.web.app.project.model.Course;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:testAppContext.xml", "classpath:testContext.xml"})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class CourseControllerIntegrationTest {
	
		@Autowired
	    private WebApplicationContext wac;

	    private MockMvc mockMvc;

	    @Before
	    public void setup() {
	        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	    }
	    
	    
	    @Test
	    public void findByIdTest_Successfull() throws Exception{
	    	
	    	mockMvc.perform(get("/course/find/{id}",1L))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.courseId", is(1)))
			.andExpect(jsonPath("$.courseName", is("Calculus 1")))
			.andExpect(jsonPath("$.fund_classes", is("3+2+1")))
			.andExpect(jsonPath("$.contentOfProgram", is("Content of program 1")))
			.andExpect(jsonPath("$.goalOfProgram", is("Goal of program 1")))
			.andExpect(jsonPath("$.language", is("Macedonian")))
			.andExpect(jsonPath("$.professor", is("Verica Bakeva")));
	    	
	    }
	    
	    
	    @Test
	    public void findByIdTest_NotSuccessfull() throws Exception{
	    	
	    	mockMvc.perform(get("/course/find/{id}",10L))
	    		.andExpect(status().isNotFound());
	    	
	    }
	    
	    @Test
	    public void getAllCoursesTest_Successfull_TwoCourses_Returned() throws Exception{
	    		
	    	mockMvc.perform(get("/course/all"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$",hasSize(2)))
			.andExpect(jsonPath("$[0].courseId", is(1)))
			.andExpect(jsonPath("$[0].courseName", is("Calculus 1")))
			.andExpect(jsonPath("$[0].fund_classes", is("3+2+1")))
			.andExpect(jsonPath("$[0].contentOfProgram", is("Content of program 1")))
			.andExpect(jsonPath("$[0].goalOfProgram", is("Goal of program 1")))
			.andExpect(jsonPath("$[0].language", is("Macedonian")))
			.andExpect(jsonPath("$[0].professor", is("Verica Bakeva")))
			
			.andExpect(jsonPath("$[1].courseId", is(2)))
			.andExpect(jsonPath("$[1].courseName", is("Calculus 2")))
			.andExpect(jsonPath("$[1].fund_classes", is("3+2+1")))
			.andExpect(jsonPath("$[1].contentOfProgram", is("Content of program for Calculus 2")))
			.andExpect(jsonPath("$[1].goalOfProgram", is("Goal of program for Calculus 2")))
			.andExpect(jsonPath("$[1].language", is("Macedonian")))
			.andExpect(jsonPath("$[1].professor", is("Biljana Tojtovska")));

	    }
	    // AddingCourseTest_Successfull
	    
	    @Test
	    public void addCourseTest_Successfull() throws IOException, Exception{
	    	
	    		Course	course2=new Course();
	    				course2.setCourseName("Web programiranje");
	    				course2.setFund_classes("2+2+1");
	    				course2.setContentOfProgram("Content of program for Web programming");
	    				course2.setGoalOfProgram("Goal of program for Web programming");
	    				course2.setLanguage("English");
	    				course2.setProfessor("Dimitar Trajanov");
	    				
	    				
	    				
	    				mockMvc.perform(post("/course/addCourse")
	    						.contentType(MediaType.APPLICATION_JSON)
	    						.content(TestUtil.convertObjectToJsonBytes(course2))
	    					)
	    					.andExpect(status().isOk())
	    					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
	    					
	    					.andExpect(jsonPath("$.status",is("ok")))
	    					.andExpect(jsonPath("$.message",is("Course successfully added")));
	    					
	    }
	    
	    @Test
	    public void addCourseTest_NotSuccessfull() throws IOException, Exception{
	    	
	    		Course	course2=new Course();
	    				course2.setCourseName("Web programiranje");
	    				course2.setFund_classes("2+2+1");
	    				course2.setContentOfProgram("Content of program for Web programming");
	    				course2.setGoalOfProgram("Goal of program for Web programming");
	    				course2.setLanguage(null);
	    				course2.setProfessor("Dimitar Trajanov");
	    				
	    				
	    				
	    				mockMvc.perform(post("/course/addCourse")
	    						.contentType(MediaType.APPLICATION_JSON)
	    						.content(TestUtil.convertObjectToJsonBytes(course2))
	    					)
	    					.andExpect(status().isOk())
	    					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
	    					
	    					.andExpect(jsonPath("$.status",is("err")))
	    					.andExpect(jsonPath("$.message",is("Course is not successfully added")));
	    					
	    }
	    @Test
	    public void updateCourseTest_Successfull() throws IOException, Exception{
	    	
			  Course course2=new Course();
					course2.setCourseId(1L);
					course2.setCourseName("Calculus 2");
					course2.setFund_classes("3+2+1");
					course2.setContentOfProgram("Content of program for Calculus 2");
					course2.setGoalOfProgram("Goal of program for Calculus 2");
					course2.setLanguage("Macedonian");
					course2.setProfessor("Aleksandra Popovska Mitrovic");
	    				
	    				
	    				
	    				mockMvc.perform(post("/course/updateCourse")
	    						.contentType(MediaType.APPLICATION_JSON)
	    						.content(TestUtil.convertObjectToJsonBytes(course2))
	    					)
	    					.andExpect(status().isOk())
	    					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
	    					
	    					.andExpect(jsonPath("$.status",is("ok")))
	    					.andExpect(jsonPath("$.message",is("Course successfully updated")));
	    					
	    }
	    
	    @Test
	    public void updateCourseTest_NotSuccessfull() throws IOException, Exception{
	    	
			  Course course2=new Course();
					course2.setCourseId(1L);
					course2.setCourseName("Calculus 2");
					course2.setFund_classes("3+2+1");
					course2.setContentOfProgram("Content of program for Calculus 2");
					course2.setGoalOfProgram("Goal of program for Calculus 2");
					course2.setLanguage("");
					course2.setProfessor("Biljana Tojtovska");
	    				
	    				
	    				
	    				mockMvc.perform(post("/course/updateCourse")
	    						.contentType(MediaType.APPLICATION_JSON)
	    						.content(TestUtil.convertObjectToJsonBytes(course2))
	    					)
	    					.andExpect(status().isOk())
	    					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
	    					
	    					.andExpect(jsonPath("$.status",is("err")))
	    					.andExpect(jsonPath("$.message",is("Course is not successfully updated")));
	    					
	    }
	    @Test
		public void deleteCourseTest_Successfull() throws Exception{
				
			
					
					mockMvc.perform(delete("/course/delete/{id}",1L)
							
						)
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.status",is("ok")))
						.andExpect(jsonPath("$.message",is("Course successfully deleted")));
						
					
							
		}	
	    
	    @Test
		public void deleteCourseTest_NotSuccessfull() throws Exception{
				
					
					mockMvc.perform(delete("/course/delete/{id}",10L))
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.status",is("err")))
						.andExpect(jsonPath("$.message",is("Course is not successfully deleted")));
						
						
		}	
	    
	    @Test
		public void addStudentCourseTest_Successfull() throws Exception{
	    	
	    	Student student1=new Student();
				student1.setId(1L);
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("078000000");
				student1.setEmail("Mitre@yahoo.com");
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				student1.setEMBG("123456987");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
					
					
				
					// Course 1
				Course course1=new Course();
					course1.setCourseId(1L);
					course1.setCourseName("Calculus 1");
					course1.setContentOfProgram("Content of program Calculus 1");
					course1.setFund_classes("3+2+1");
					course1.setGoalOfProgram("Goal of program Calculus 1");
					course1.setLanguage("English");
					course1.setProfessor("Verica Bakeva");
					
				// StudentCourse
				StudentCourse studentCourse1=new StudentCourse();
					studentCourse1.setCourse(course1);
					studentCourse1.setStudent(student1);
					studentCourse1.setGrade(10.00);
					studentCourse1.setSchoolYear(2013);
					studentCourse1.setSemester("WINTER");
					
					
					mockMvc.perform(post("/course/addStudentCourse")
							.contentType(MediaType.APPLICATION_JSON)
							.content(TestUtil.convertObjectToJsonBytes(studentCourse1))
						)
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						
						.andExpect(jsonPath("$.status",is("ok")))
						.andExpect(jsonPath("$.message",is("Student course data successfully added")));
	    }
	    
	    
	    @Test
		public void addStudentCourseTest_NotSuccessfull() throws Exception{

				
					
					
					// Course 1
	    	Course course1=new Course();
					course1.setCourseId(2L);
					course1.setCourseName("Calculus 2");
					course1.setContentOfProgram("Content of program for Calculus 2");
					course1.setFund_classes("3+2+1");
					course1.setGoalOfProgram("Goal of program for Calculus 2");
					course1.setLanguage("Macedonian");
					course1.setProfessor("Biljana Tojtovska");
					
				// StudentCourse
				StudentCourse studentCourse1=new StudentCourse();
					studentCourse1.setCourse(course1);
					studentCourse1.setStudent(null);
					studentCourse1.setGrade(9.00);
					studentCourse1.setSchoolYear(2014);
					studentCourse1.setSemester("SUMMER");
					
					
					mockMvc.perform(post("/course/addStudentCourse")
							.contentType(MediaType.APPLICATION_JSON)
							.content(TestUtil.convertObjectToJsonBytes(studentCourse1))
						)
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						
						.andExpect(jsonPath("$.status",is("err")))
						.andExpect(jsonPath("$.message",is("Student course data is not successfully added")));			
		}	
	    
	    @Test 
		public void getAllStudentCoursesTest_Successfull() throws Exception{
	    	
					mockMvc.perform(get("/course/allStudentCourses/{id}",2L))
							.andExpect(status().isOk())
							.andExpect(content().contentType(MediaType.APPLICATION_JSON))
							.andExpect(jsonPath("$",hasSize(2)))
							
							// Student
							.andExpect(jsonPath("$[0].student.id",is(2)))	
					 		.andExpect(jsonPath("$[0].student.index",is(131072)))
					 		.andExpect(jsonPath("$[0].student.yearOfEnrollment",is("2013")))
					 		.andExpect(jsonPath("$[0].student.status",is("REGULAR")))
					 		.andExpect(jsonPath("$[0].student.cycle",is("FIRST")))
					 		.andExpect(jsonPath("$[0].student.quota",is("STATE")))
					 		.andExpect(jsonPath("$[0].student.studiesProgram",is("KNI")))
					 		.andExpect(jsonPath("$[0].student.placeOfLiving",is("Skopje")))
					 		.andExpect(jsonPath("$[0].student.municipalityOfLiving",is("Skopje")))
					 		.andExpect(jsonPath("$[0].student.country",is("Macedonia")))
					 		.andExpect(jsonPath("$[0].student.address",is("Rugjer Boshkovikj")))
					 		.andExpect(jsonPath("$[0].student.phone",is("078000000")))
					 		.andExpect(jsonPath("$[0].student.email",is("ilija@yahoo.com")))	
					 		.andExpect(jsonPath("$[0].student.name",is("Ilija")))
					 		.andExpect(jsonPath("$[0].student.surname",is("Ilijev")))
					 		.andExpect(jsonPath("$[0].student.embg",is("123456789")))
					 		.andExpect(jsonPath("$[0].student.dateOfBirth",is(new Date("09/05/2016").getTime())))	
					 		.andExpect(jsonPath("$[0].student.nationality",is("Macedonian")))
					 		.andExpect(jsonPath("$[0].student.gender",is("MALE")))
					 		.andExpect(jsonPath("$[0].student.citizenship",is("Macedonian")))
					 		.andExpect(jsonPath("$[0].student.placeOfBirth",is("Kavadarci")))
					 		.andExpect(jsonPath("$[0].student.municipalityOfBirth",is("Kavadarci")))
					 		.andExpect(jsonPath("$[0].student.countryOfBirth",is("Macedonia")))
					 		
							 	//Course 1
							.andExpect(jsonPath("$[0].course.courseId", is(1)))
							.andExpect(jsonPath("$[0].course.courseName", is("Calculus 1")))
							.andExpect(jsonPath("$[0].course.fund_classes", is("3+2+1")))
							.andExpect(jsonPath("$[0].course.contentOfProgram", is("Content of program 1")))
							.andExpect(jsonPath("$[0].course.goalOfProgram", is("Goal of program 1")))
							.andExpect(jsonPath("$[0].course.language", is("Macedonian")))
							.andExpect(jsonPath("$[0].course.professor", is("Verica Bakeva")))
							
								
							.andExpect(jsonPath("$[0].grade", is(10.00)))
							.andExpect(jsonPath("$[0].schoolYear", is(2013)))
							.andExpect(jsonPath("$[0].semester", is("WINTER")))
							
								// Student 1
							.andExpect(jsonPath("$[1].student.id",is(2)))	
					 		.andExpect(jsonPath("$[1].student.index",is(131072)))
					 		.andExpect(jsonPath("$[1].student.yearOfEnrollment",is("2013")))
					 		.andExpect(jsonPath("$[1].student.status",is("REGULAR")))
					 		.andExpect(jsonPath("$[1].student.cycle",is("FIRST")))
					 		.andExpect(jsonPath("$[1].student.quota",is("STATE")))
					 		.andExpect(jsonPath("$[1].student.studiesProgram",is("KNI")))
					 		.andExpect(jsonPath("$[1].student.placeOfLiving",is("Skopje")))
					 		.andExpect(jsonPath("$[1].student.municipalityOfLiving",is("Skopje")))
					 		.andExpect(jsonPath("$[1].student.country",is("Macedonia")))
					 		.andExpect(jsonPath("$[1].student.address",is("Rugjer Boshkovikj")))
					 		.andExpect(jsonPath("$[1].student.phone",is("078000000")))
					 		.andExpect(jsonPath("$[1].student.email",is("ilija@yahoo.com")))	
					 		.andExpect(jsonPath("$[1].student.name",is("Ilija")))
					 		.andExpect(jsonPath("$[1].student.surname",is("Ilijev")))
					 		.andExpect(jsonPath("$[1].student.embg",is("123456789")))
					 		.andExpect(jsonPath("$[1].student.dateOfBirth",is(new Date("09/05/2016").getTime())))	
					 		.andExpect(jsonPath("$[1].student.nationality",is("Macedonian")))
					 		.andExpect(jsonPath("$[1].student.gender",is("MALE")))
					 		.andExpect(jsonPath("$[1].student.citizenship",is("Macedonian")))
					 		.andExpect(jsonPath("$[1].student.placeOfBirth",is("Kavadarci")))
					 		.andExpect(jsonPath("$[1].student.municipalityOfBirth",is("Kavadarci")))
					 		.andExpect(jsonPath("$[1].student.countryOfBirth",is("Macedonia")))
					 		
								// Course 2
							.andExpect(jsonPath("$[1].course.courseId", is(2)))
							.andExpect(jsonPath("$[1].course.courseName", is("Calculus 2")))
							.andExpect(jsonPath("$[1].course.fund_classes", is("3+2+1")))
							.andExpect(jsonPath("$[1].course.contentOfProgram", is("Content of program for Calculus 2")))
							.andExpect(jsonPath("$[1].course.goalOfProgram", is("Goal of program for Calculus 2")))
							.andExpect(jsonPath("$[1].course.language", is("Macedonian")))
							.andExpect(jsonPath("$[1].course.professor", is("Biljana Tojtovska")))
							
								//Student course data
							.andExpect(jsonPath("$[1].grade", is(8.00)))
							.andExpect(jsonPath("$[1].schoolYear", is(2014)))
							.andExpect(jsonPath("$[1].semester", is("SUMMER")));
					
					
		}
	    
	
		
}
