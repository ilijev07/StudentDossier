package com.web.app.project.dao.test;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.web.app.project.dao.ICourseDao;
import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;
import com.web.app.project.model.Course;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;


import static org.junit.Assert.*;

import java.io.IOException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;



@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:testAppContext.xml", "classpath:testContext.xml"})
@Transactional
@TransactionConfiguration(defaultRollback = true)

public class CourseDaoIntegrationTest {
	
	@Resource(name="courseDaoImpl")
	ICourseDao courseDao;
	
	
	
	@Test
	public void findByIdTest_Successfull(){
		
		Course course1=courseDao.findById(1L);
		
		
		assertNotNull(course1);
		assertEquals(course1.getCourseName(),"Calculus 1");
		assertEquals(course1.getContentOfProgram(),"Content of program 1");
		assertEquals(course1.getFund_classes(),"3+2+1");
		assertEquals(course1.getGoalOfProgram(),"Goal of program 1");
		assertEquals(course1.getLanguage(),"Macedonian");
		assertEquals(course1.getProfessor(),"Verica Bakeva");

	}
	@Test
	public void findByIdTest_NotSuccessfull(){
		
		Course course1=courseDao.findById(10L);
		
		assertNull(course1);
	
	}
	
	@Test
	public void getAllCoursesTest_Successfull(){
		
		List<Course> courses=courseDao.getAllCourses();
		
		
		assertNotNull(courses);
		
		assertEquals(courses.get(0).getCourseName(),"Calculus 1");
		assertEquals(courses.get(0).getContentOfProgram(),"Content of program 1");
		assertEquals(courses.get(0).getFund_classes(),"3+2+1");
		assertEquals(courses.get(0).getGoalOfProgram(),"Goal of program 1");
		assertEquals(courses.get(0).getLanguage(),"Macedonian");
		assertEquals(courses.get(0).getProfessor(),"Verica Bakeva");
		
		assertEquals(courses.get(1).getCourseName(),"Calculus 2");
		assertEquals(courses.get(1).getContentOfProgram(),"Content of program for Calculus 2");
		assertEquals(courses.get(1).getFund_classes(),"3+2+1");
		assertEquals(courses.get(1).getGoalOfProgram(),"Goal of program for Calculus 2");
		assertEquals(courses.get(1).getLanguage(),"Macedonian");
		assertEquals(courses.get(1).getProfessor(),"Biljana Tojtovska");

	}
	
	 @Test
	 public void addCourseTest_Successfull() throws IOException, Exception{
	    	
	    		Course	course2=new Course();
	    				course2.setCourseName("Web programiranje");
	    				course2.setFund_classes("2+2+1");
	    				course2.setContentOfProgram("Content of program for Web programming");
	    				course2.setGoalOfProgram("Goal of program for Web programming");
	    				course2.setLanguage("English");
	    				course2.setProfessor("Dimitar Trajanov");
	    				
	    				
	    	  HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.addCourse(course2);
	    	   
	    	   assertNotNull(hashMap);
	    	   
	    	   assertEquals(hashMap.get("status"),"ok");
	    	   assertEquals(hashMap.get("message"),"Course successfully added");
				
	 }
	 @Test
	 public void addCourseTest_NotSuccessfull() throws IOException, Exception{
	    	
	    		Course	course2=new Course();
	    				course2.setCourseName("Web programiranje");
	    				course2.setFund_classes("2+2+1");
	    				course2.setContentOfProgram("Content of program for Web programming");
	    				course2.setGoalOfProgram("Goal of program for Web programming");
	    				course2.setLanguage("English");
	    				course2.setProfessor(null);
	    				
	    				
	    	  HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.addCourse(course2);
	    	   
	    	   assertNotNull(hashMap);
	    	   
	    	   assertEquals(hashMap.get("status"),"err");
	    	   assertEquals(hashMap.get("message"),"Course is not successfully added");
				
	  }
	 
	 @Test
	 public void updateCourseTest_Successfull() throws IOException, Exception{
	    	
			  Course course2=courseDao.findById(1L);
					
					course2.setProfessor("Aleksandra Popovska Mitrovic");
	    				
	    				
					HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.updateCourse(course2);		
					
					assertNotNull(hashMap);
					 
					assertEquals(hashMap.get("status"),"ok");
			    	assertEquals(hashMap.get("message"),"Course successfully updated");
	    			
	    					
	    }
	 
	 @Test
	 public void updateCourseTest_NotSuccessfull() throws IOException, Exception{
	    	
			  Course course2=courseDao.findById(1L);
					
					course2.setProfessor(null);
	    				
	    				
					HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.updateCourse(course2);		
					
					assertNotNull(hashMap);
					 
					assertEquals(hashMap.get("status"),"err");
			    	assertEquals(hashMap.get("message"),"Course is not successfully updated");
 					
	    }
	 	@Test
		public void deleteCourseTest_Successfull() throws Exception{
					
	 		HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.deleteCourse(1L);
	 		
	 		
	 		assertNotNull(hashMap);
			 
			assertEquals(hashMap.get("status"),"ok");
	    	assertEquals(hashMap.get("message"),"Course successfully deleted");
								
		}	
	 	
	 	@Test
		public void deleteCourseTest_NotSuccessfull() throws Exception{
					
	 		HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.deleteCourse(10L);
	 		
	 		
	 		assertNotNull(hashMap);
			 
			assertEquals(hashMap.get("status"),"err");
	    	assertEquals(hashMap.get("message"),"Course is not successfully deleted");
								
		}	
	 	 @Test
		public void addStudentCourseTest_Successfull() throws Exception{
		    	
		    	Student student1=new Student();
					student1.setId(1L);
					student1.setIndex(131000);
					student1.setYearOfEnrollment("2013");
					student1.setStatus(Status.REGULAR);
					student1.setCycle(StudiesCycle.FIRST);
					student1.setQuota(Quota.PRIVATE);
					student1.setStudiesProgram("KNI");
					student1.setPlaceOfLiving("Skopje");
					student1.setMunicipalityOfLiving("Karposh");
					student1.setCountry("Macedonia");
					student1.setAddress("Bihakjka");
					student1.setPhone("078000000");
					student1.setEmail("Mitre@yahoo.com");
					
					// Personal info
					student1.setName("Mitre");
					student1.setSurname("Mitrev");
					student1.setEMBG("123456987");
					student1.setDateOfBirth(new Date("01/02/1994"));
					student1.setNationality("Macedonian");
					student1.setGender(Gender.MALE);
					student1.setCitizenship("Macedonian");
					student1.setPlaceOfBirth("Skopje");
					student1.setMunicipalityOfBirth("Skopje");
					student1.setCountryOfBirth("Macedonia");
						
						
					
						// Course 1
					Course course1=new Course();
						course1.setCourseId(1L);
						course1.setCourseName("Calculus 1");
						course1.setContentOfProgram("Content of program Calculus 1");
						course1.setFund_classes("3+2+1");
						course1.setGoalOfProgram("Goal of program Calculus 1");
						course1.setLanguage("English");
						course1.setProfessor("Verica Bakeva");
						
					// StudentCourse
					StudentCourse studentCourse1=new StudentCourse();
						studentCourse1.setCourse(course1);
						studentCourse1.setStudent(student1);
						studentCourse1.setGrade(10.00);
						studentCourse1.setSchoolYear(2013);
						studentCourse1.setSemester("WINTER");
						
						HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.addStudentData(studentCourse1);
				 		
				 		
				 		assertNotNull(hashMap);
						 
						assertEquals(hashMap.get("status"),"ok");
				    	assertEquals(hashMap.get("message"),"Student course data successfully added");
						
		    }
	 	 
	 	@Test
		public void addStudentCourseTest_NotSuccessfull() throws Exception{

					
						
						
						// Course 1
		    	Course course1=new Course();
						course1.setCourseId(2L);
						course1.setCourseName("Calculus 2");
						course1.setContentOfProgram("Content of program for Calculus 2");
						course1.setFund_classes("3+2+1");
						course1.setGoalOfProgram("Goal of program for Calculus 2");
						course1.setLanguage("Macedonian");
						course1.setProfessor("Biljana Tojtovska");
						
					// StudentCourse
					StudentCourse studentCourse1=new StudentCourse();
						studentCourse1.setCourse(course1);
						studentCourse1.setStudent(null);
						studentCourse1.setGrade(9.00);
						studentCourse1.setSchoolYear(2014);
						studentCourse1.setSemester("SUMMER");
						
						HashMap<String,String> hashMap= (HashMap<String, String>) courseDao.addStudentData(studentCourse1);
				 		
				 		
				 		assertNotNull(hashMap);
						 
						assertEquals(hashMap.get("status"),"err");
				    	assertEquals(hashMap.get("message"),"Student course data is not successfully added");
						
							
			}
	 	
	 	@Test 
		public void getAllStudentCoursesTest_Successfull() throws Exception{
	    	
			 		List<StudentCourse> studentCourses=courseDao.getAllStudentCourses(2L);
			 		
			 		
			 		assertNotNull(studentCourses);
						
			 		// Assertion of student of first Student Course record
			 		assertEquals(studentCourses.get(0).getStudent().getId(),2L);
			 		assertEquals(studentCourses.get(0).getStudent().getIndex(),131072);
			 		assertEquals(studentCourses.get(0).getStudent().getYearOfEnrollment(),"2013");
			 		assertEquals(studentCourses.get(0).getStudent().getStatus(),Status.REGULAR);
			 		assertEquals(studentCourses.get(0).getStudent().getCycle(),StudiesCycle.FIRST);
			 		assertEquals(studentCourses.get(0).getStudent().getQuota(),Quota.STATE);
			 		assertEquals(studentCourses.get(0).getStudent().getStudiesProgram(),"KNI");
			 		assertEquals(studentCourses.get(0).getStudent().getPlaceOfLiving(),"Skopje");
			 		assertEquals(studentCourses.get(0).getStudent().getMunicipalityOfLiving(),"Skopje");
			 		assertEquals(studentCourses.get(0).getStudent().getCountry(),"Macedonia");
			 		assertEquals(studentCourses.get(0).getStudent().getAddress(),"Rugjer Boshkovikj");
			 		assertEquals(studentCourses.get(0).getStudent().getPhone(),"078000000");
			 		assertEquals(studentCourses.get(0).getStudent().getEmail(),"ilija@yahoo.com");
			 		assertEquals(studentCourses.get(0).getStudent().getName(),"Ilija");
			 		assertEquals(studentCourses.get(0).getStudent().getSurname(),"Ilijev");
			 		assertEquals(studentCourses.get(0).getStudent().getEMBG(),"123456789");
			 		assertEquals(studentCourses.get(0).getStudent().getDateOfBirth().getTime(),new Date("09/05/2016").getTime());
			 		assertEquals(studentCourses.get(0).getStudent().getNationality(),"Macedonian");
			 		assertEquals(studentCourses.get(0).getStudent().getGender(),Gender.MALE);
			 		assertEquals(studentCourses.get(0).getStudent().getCitizenship(),"Macedonian");
			 		assertEquals(studentCourses.get(0).getStudent().getPlaceOfBirth(),"Kavadarci");
			 		assertEquals(studentCourses.get(0).getStudent().getMunicipalityOfBirth(),"Kavadarci");
			 		assertEquals(studentCourses.get(0).getStudent().getCountryOfBirth(),"Macedonia");
			 		
			 		// Assertion of course of first Student Course record
			 		assertEquals(studentCourses.get(0).getCourse().getCourseId(),1L);
			 		assertEquals(studentCourses.get(0).getCourse().getCourseName(),"Calculus 1");
			 		assertEquals(studentCourses.get(0).getCourse().getFund_classes(),"3+2+1");
			 		assertEquals(studentCourses.get(0).getCourse().getContentOfProgram(),"Content of program 1");
			 		assertEquals(studentCourses.get(0).getCourse().getGoalOfProgram(),"Goal of program 1");
			 		assertEquals(studentCourses.get(0).getCourse().getLanguage(),"Macedonian");
			 		assertEquals(studentCourses.get(0).getCourse().getProfessor(),"Verica Bakeva");
			 		
			 		
			 		assertEquals(studentCourses.get(0).getGrade(),10.00,0.0002);
			 		assertEquals(studentCourses.get(0).getSchoolYear(),2013);
			 		assertEquals(studentCourses.get(0).getSemester(),"WINTER");
			 	
					 		
			 		assertEquals(studentCourses.get(1).getStudent().getId(),2L);
			 		assertEquals(studentCourses.get(1).getStudent().getIndex(),131072);
			 		assertEquals(studentCourses.get(1).getStudent().getYearOfEnrollment(),"2013");
			 		assertEquals(studentCourses.get(1).getStudent().getStatus(),Status.REGULAR);
			 		assertEquals(studentCourses.get(1).getStudent().getCycle(),StudiesCycle.FIRST);
			 		assertEquals(studentCourses.get(1).getStudent().getQuota(),Quota.STATE);
			 		assertEquals(studentCourses.get(1).getStudent().getStudiesProgram(),"KNI");
			 		assertEquals(studentCourses.get(1).getStudent().getPlaceOfLiving(),"Skopje");
			 		assertEquals(studentCourses.get(1).getStudent().getMunicipalityOfLiving(),"Skopje");
			 		assertEquals(studentCourses.get(1).getStudent().getCountry(),"Macedonia");
			 		assertEquals(studentCourses.get(1).getStudent().getAddress(),"Rugjer Boshkovikj");
			 		assertEquals(studentCourses.get(1).getStudent().getPhone(),"078000000");
			 		assertEquals(studentCourses.get(1).getStudent().getEmail(),"ilija@yahoo.com");
			 		assertEquals(studentCourses.get(1).getStudent().getName(),"Ilija");
			 		assertEquals(studentCourses.get(1).getStudent().getSurname(),"Ilijev");
			 		assertEquals(studentCourses.get(1).getStudent().getEMBG(),"123456789");
			 		assertEquals(studentCourses.get(1).getStudent().getDateOfBirth().getTime(),new Date("09/05/2016").getTime());
			 		assertEquals(studentCourses.get(1).getStudent().getNationality(),"Macedonian");
			 		assertEquals(studentCourses.get(1).getStudent().getGender(),Gender.MALE);
			 		assertEquals(studentCourses.get(1).getStudent().getCitizenship(),"Macedonian");
			 		assertEquals(studentCourses.get(1).getStudent().getPlaceOfBirth(),"Kavadarci");
			 		assertEquals(studentCourses.get(1).getStudent().getMunicipalityOfBirth(),"Kavadarci");
			 		assertEquals(studentCourses.get(1).getStudent().getCountryOfBirth(),"Macedonia");		
							
			 	
			 		assertEquals(studentCourses.get(1).getCourse().getCourseId(),2L);
			 		assertEquals(studentCourses.get(1).getCourse().getCourseName(),"Calculus 2");
			 		assertEquals(studentCourses.get(1).getCourse().getFund_classes(),"3+2+1");
			 		assertEquals(studentCourses.get(1).getCourse().getContentOfProgram(),"Content of program for Calculus 2");
			 		assertEquals(studentCourses.get(1).getCourse().getGoalOfProgram(),"Goal of program for Calculus 2");
			 		assertEquals(studentCourses.get(1).getCourse().getLanguage(),"Macedonian");
			 		assertEquals(studentCourses.get(1).getCourse().getProfessor(),"Biljana Tojtovska");		
			 		
			 		
			 		assertEquals(studentCourses.get(1).getGrade(),8.00,0.0002);
			 		assertEquals(studentCourses.get(1).getSchoolYear(),2014);
			 		assertEquals(studentCourses.get(1).getSemester(),"SUMMER");
					
					
		}
		
	    
	    

}
