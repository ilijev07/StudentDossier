package com.web.app.project.dao.test;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;


import com.web.app.project.dao.IStudentDao;
import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;

import com.web.app.project.model.Student;


import static org.junit.Assert.*;



import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;



@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:testAppContext.xml", "classpath:testContext.xml"})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class StudentDaoIntegratingTest {
	
	
	@Resource(name="studentDaoImpl")
	IStudentDao studentDao;
	
	
	@Test
    public void getAllStudentsTest_Successfull() throws Exception{
		
		List<Student> students=studentDao.getAllStudents();
		assertNotNull(students);
		
		// Assertion of student of first Student Course record
 		assertEquals(students.get(0).getId(),2L);
 		assertEquals(students.get(0).getIndex(),131072);
 		assertEquals(students.get(0).getYearOfEnrollment(),"2013");
 		assertEquals(students.get(0).getStatus(),Status.REGULAR);
 		assertEquals(students.get(0).getCycle(),StudiesCycle.FIRST);
 		assertEquals(students.get(0).getQuota(),Quota.STATE);
 		assertEquals(students.get(0).getStudiesProgram(),"KNI");
 		assertEquals(students.get(0).getPlaceOfLiving(),"Skopje");
 		assertEquals(students.get(0).getMunicipalityOfLiving(),"Skopje");
 		assertEquals(students.get(0).getCountry(),"Macedonia");
 		assertEquals(students.get(0).getAddress(),"Rugjer Boshkovikj");
 		assertEquals(students.get(0).getPhone(),"078000000");
 		assertEquals(students.get(0).getEmail(),"ilija@yahoo.com");
 		assertEquals(students.get(0).getName(),"Ilija");
 		assertEquals(students.get(0).getSurname(),"Ilijev");
 		assertEquals(students.get(0).getEMBG(),"123456789");
 		assertEquals(students.get(0).getDateOfBirth().getTime(),new Date("09/05/2016").getTime());
 		assertEquals(students.get(0).getNationality(),"Macedonian");
 		assertEquals(students.get(0).getGender(),Gender.MALE);
 		assertEquals(students.get(0).getCitizenship(),"Macedonian");
 		assertEquals(students.get(0).getPlaceOfBirth(),"Kavadarci");
 		assertEquals(students.get(0).getMunicipalityOfBirth(),"Kavadarci");
 		assertEquals(students.get(0).getCountryOfBirth(),"Macedonia");
 		
 		
 		
 		// Assertion of student of first Student Course record
 		assertEquals(students.get(1).getId(),3L);
 		assertEquals(students.get(1).getIndex(),131027);
 		assertEquals(students.get(1).getYearOfEnrollment(),"2013");
 		assertEquals(students.get(1).getStatus(),Status.REGULAR);
 		assertEquals(students.get(1).getCycle(),StudiesCycle.FIRST);
 		assertEquals(students.get(1).getQuota(),Quota.STATE);
 		assertEquals(students.get(1).getStudiesProgram(),"KNI");
 		assertEquals(students.get(1).getPlaceOfLiving(),"Skopje");
 		assertEquals(students.get(1).getMunicipalityOfLiving(),"Karposh");
 		assertEquals(students.get(1).getCountry(),"Macedonia");
 		assertEquals(students.get(1).getAddress(),"Rugjer Boshkovikj");
 		assertEquals(students.get(1).getPhone(),"076000000");
 		assertEquals(students.get(1).getEmail(),"antonio@yahoo.com");
 		assertEquals(students.get(1).getName(),"Antonio");
 		assertEquals(students.get(1).getSurname(),"Todorov");
 		assertEquals(students.get(1).getEMBG(),"987456321");
 		assertEquals(students.get(1).getDateOfBirth().getTime(),new Date("03/19/1991").getTime());
 		assertEquals(students.get(1).getNationality(),"Macedonian");
 		assertEquals(students.get(1).getGender(),Gender.MALE);
 		assertEquals(students.get(1).getCitizenship(),"Macedonian");
 		assertEquals(students.get(1).getPlaceOfBirth(),"Kavadarci");
 		assertEquals(students.get(1).getMunicipalityOfBirth(),"Kavadarci");
 		assertEquals(students.get(1).getCountryOfBirth(),"Macedonia");
	
		
	
	 		
    }
    
	
	@Test
	public void findByIdTest_Successfull() throws Exception{
		
		
		Student student=studentDao.findById(3L);
		 		
		assertNotNull(student);
		
		assertEquals(student.getId(),3L);
 		assertEquals(student.getIndex(),131027);
 		assertEquals(student.getYearOfEnrollment(),"2013");
 		assertEquals(student.getStatus(),Status.REGULAR);
 		assertEquals(student.getCycle(),StudiesCycle.FIRST);
 		assertEquals(student.getQuota(),Quota.STATE);
 		assertEquals(student.getStudiesProgram(),"KNI");
 		assertEquals(student.getPlaceOfLiving(),"Skopje");
 		assertEquals(student.getMunicipalityOfLiving(),"Karposh");
 		assertEquals(student.getCountry(),"Macedonia");
 		assertEquals(student.getAddress(),"Rugjer Boshkovikj");
 		assertEquals(student.getPhone(),"076000000");
 		assertEquals(student.getEmail(),"antonio@yahoo.com");
 		assertEquals(student.getName(),"Antonio");
 		assertEquals(student.getSurname(),"Todorov");
 		assertEquals(student.getEMBG(),"987456321");
 		assertEquals(student.getDateOfBirth().getTime(),new Date("03/19/1991").getTime());
 		assertEquals(student.getNationality(),"Macedonian");
 		assertEquals(student.getGender(),Gender.MALE);
 		assertEquals(student.getCitizenship(),"Macedonian");
 		assertEquals(student.getPlaceOfBirth(),"Kavadarci");
 		assertEquals(student.getMunicipalityOfBirth(),"Kavadarci");
 		assertEquals(student.getCountryOfBirth(),"Macedonia");

	}
	
	
	@Test
	public void findByIdTest_NotSuccessfull() throws Exception{
		
		
		Student student=studentDao.findById(10L);
		 		
		assertNull(student);
	}
    
	
	@Test
	public void addStudentTest_NotSuccessfull() throws Exception{
				
			// First student
				Student student1=new Student();
					
					
					student1.setIndex(131000);
					student1.setYearOfEnrollment("2013");
					student1.setStatus(Status.REGULAR);
					student1.setCycle(StudiesCycle.FIRST);
					student1.setQuota(Quota.PRIVATE);
					student1.setStudiesProgram("KNI");
					student1.setPlaceOfLiving("Skopje");
					student1.setMunicipalityOfLiving("Karposh");
					student1.setCountry("Macedonia");
					student1.setAddress("Bihakjka");
					student1.setPhone("078000000");
					
					/// EMAIL IS NULL
					student1.setEmail(null);
					
					// Personal info
					student1.setName(null);
					student1.setSurname("Mitrev");
					student1.setEMBG("123456987");
					student1.setDateOfBirth(new Date("01/02/1994"));
					student1.setNationality("Macedonian");
					student1.setGender(Gender.MALE);
					student1.setCitizenship("Macedonian");
					student1.setPlaceOfBirth("Skopje");
					student1.setMunicipalityOfBirth("Skopje");
					student1.setCountryOfBirth("Macedonia");
					
					HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.addStudent(student1);
					
					assertEquals(hashMap.get("status"),"err");
					assertEquals(hashMap.get("message"),"Student is not added successfully");
				
		}
	
	@Test
	public void addStudentTest_Successfull() throws Exception{
			
				// First student
				Student student1=new Student();
				
				
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("072000000");
				student1.setEmail("Mitre@yahoo.com");
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				student1.setEMBG("963852741");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				
				HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.addStudent(student1);
				
				assertEquals(hashMap.get("status"),"ok");
				assertEquals(hashMap.get("message"),"Student successfully added");		
	}	
	
	 @Test
	 public void updateStudentTest_Successfull() throws Exception{			
			// First student
					Student student1=studentDao.findById(2L);	
					student1.setPlaceOfLiving("Bitola");
					student1.setMunicipalityOfLiving("Bitola");
					student1.setAddress("Bitolska");
				
					HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.updateStudent(student1);	
					assertEquals(hashMap.get("status"),"ok");
					assertEquals(hashMap.get("message"),"Student successfully updated");		
		}	
	 
	 @Test
	public void updateStudentTest_NotSuccessfull() throws Exception{		
			// First student
			Student student1=studentDao.findById(2L);
			
			student1.setMunicipalityOfLiving("");
			
			HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.updateStudent(student1);
					
			assertEquals(hashMap.get("status"),"err");
			assertEquals(hashMap.get("message"),"Student is not successfully updated");		

						
		}	
	 
	 
	 @Test
	public void deleteStudentTest_Successfull() throws Exception{
				
		 	HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.deleteStudent(3L);
			
			assertEquals(hashMap.get("status"),"ok");
			assertEquals(hashMap.get("message"),"Student successfully deleted");		
						
	}	
	
	 
	 @Test
	public void deleteStudentTest_NotSuccessfull() throws Exception{

			
		 	HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.deleteStudent(10L);
			
			assertEquals(hashMap.get("status"),"err");
			assertEquals(hashMap.get("message"),"Student is not successfully deleted");	
			
				
				
	}	
	 
	 @Test
	public void deleteStudentCourseTest_Successfull() throws Exception{
		 		
		 	HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.deleteStudentCourse(2L, 1L);
			
			assertEquals(hashMap.get("status"),"ok");
			assertEquals(hashMap.get("message"),"Student course data successfully deleted");	
					
					
	}	
	 
	 @Test
	public void deleteStudentCourseTest_NotSuccessfull() throws Exception{
			
			
		 HashMap<String, String> hashMap=(HashMap<String, String>) studentDao.deleteStudentCourse(10L, 1L);
			
			assertEquals(hashMap.get("status"),"err");
			assertEquals(hashMap.get("message"),"Student course data is not successfully deleted");	
			
				
			
			
	      		
		}	
	
	

}

