package com.web.app.project.controllers.test;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;
import com.web.app.project.helper.TestUtil;
import com.web.app.project.model.Course;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:testAppContext.xml", "classpath:testContext.xml"})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class StudentControllerIntegrationTest {
	
	
	
	@Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    
    
    @Test
    public void getAllStudentsTest_Successfull() throws Exception{
    	 mockMvc.perform(get("/student/all"))
	 		.andExpect(status().isOk())
	 		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
	 		.andExpect(jsonPath("$",hasSize(2)))
	 		.andExpect(jsonPath("$[0].id",is(2)))	
	 		.andExpect(jsonPath("$[0].index",is(131072)))
	 		.andExpect(jsonPath("$[0].yearOfEnrollment",is("2013")))
	 		.andExpect(jsonPath("$[0].status",is("REGULAR")))
	 		.andExpect(jsonPath("$[0].cycle",is("FIRST")))
	 		.andExpect(jsonPath("$[0].quota",is("STATE")))
	 		.andExpect(jsonPath("$[0].studiesProgram",is("KNI")))
	 		.andExpect(jsonPath("$[0].placeOfLiving",is("Skopje")))
	 		.andExpect(jsonPath("$[0].municipalityOfLiving",is("Skopje")))
	 		.andExpect(jsonPath("$[0].country",is("Macedonia")))
	 		.andExpect(jsonPath("$[0].address",is("Rugjer Boshkovikj")))	
	 		.andExpect(jsonPath("$[0].phone",is("078000000")))
	 		.andExpect(jsonPath("$[0].email",is("ilija@yahoo.com")))
	 		.andExpect(jsonPath("$[0].name",is("Ilija")))
	 		.andExpect(jsonPath("$[0].surname",is("Ilijev")))
	 		.andExpect(jsonPath("$[0].embg",is("123456789")))
	 		.andExpect(jsonPath("$[0].dateOfBirth",is(new Date("09/05/2016").getTime())))	
	 		.andExpect(jsonPath("$[0].nationality",is("Macedonian")))
	 		.andExpect(jsonPath("$[0].gender",is("MALE")))
	 		.andExpect(jsonPath("$[0].citizenship",is("Macedonian")))
	 		.andExpect(jsonPath("$[0].placeOfBirth",is("Kavadarci")))
	 		.andExpect(jsonPath("$[0].municipalityOfBirth",is("Kavadarci")))
	 		.andExpect(jsonPath("$[0].countryOfBirth",is("Macedonia")))
	 		
	 			// Student 2
	 		.andExpect(jsonPath("$[1].id",is(3)))	
	 		.andExpect(jsonPath("$[1].index",is(131027)))
	 		.andExpect(jsonPath("$[1].yearOfEnrollment",is("2013")))
	 		.andExpect(jsonPath("$[1].status",is("REGULAR")))
	 		.andExpect(jsonPath("$[1].cycle",is("FIRST")))
	 		.andExpect(jsonPath("$[1].quota",is("STATE")))
	 		.andExpect(jsonPath("$[1].studiesProgram",is("KNI")))
	 		.andExpect(jsonPath("$[1].placeOfLiving",is("Skopje")))
	 		.andExpect(jsonPath("$[1].municipalityOfLiving",is("Karposh")))
	 		.andExpect(jsonPath("$[1].country",is("Macedonia")))
	 		.andExpect(jsonPath("$[1].address",is("Rugjer Boshkovikj"))) 		
	 		.andExpect(jsonPath("$[1].phone",is("076000000")))
	 		.andExpect(jsonPath("$[1].email",is("antonio@yahoo.com")))		
	 		.andExpect(jsonPath("$[1].name",is("Antonio")))
	 		.andExpect(jsonPath("$[1].surname",is("Todorov")))
	 		.andExpect(jsonPath("$[1].embg",is("987456321")))
	 		.andExpect(jsonPath("$[1].dateOfBirth",is(new Date("03/19/1991").getTime())))	 		
	 		.andExpect(jsonPath("$[1].nationality",is("Macedonian")))
	 		.andExpect(jsonPath("$[1].gender",is("MALE")))
	 		.andExpect(jsonPath("$[1].citizenship",is("Macedonian")))
	 		.andExpect(jsonPath("$[1].placeOfBirth",is("Kavadarci")))
	 		.andExpect(jsonPath("$[1].municipalityOfBirth",is("Kavadarci")))
	 		.andExpect(jsonPath("$[1].countryOfBirth",is("Macedonia")));
	 		
    }
    
    
    @Test
	public void findByIdTest_Successfull() throws Exception{
		
		
		 mockMvc.perform(get("/student//find/{id}",3L))
		 		.andExpect(status().isOk())
		 		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		 		.andExpect(jsonPath("$.id",is(3)))	
		 		.andExpect(jsonPath("$.index",is(131027)))
		 		.andExpect(jsonPath("$.yearOfEnrollment",is("2013")))
		 		.andExpect(jsonPath("$.status",is("REGULAR")))
		 		.andExpect(jsonPath("$.cycle",is("FIRST")))
		 		.andExpect(jsonPath("$.quota",is("STATE")))
		 		.andExpect(jsonPath("$.studiesProgram",is("KNI")))
		 		.andExpect(jsonPath("$.placeOfLiving",is("Skopje")))
		 		.andExpect(jsonPath("$.municipalityOfLiving",is("Karposh")))
		 		.andExpect(jsonPath("$.country",is("Macedonia")))
		 		.andExpect(jsonPath("$.address",is("Rugjer Boshkovikj"))) 		
		 		.andExpect(jsonPath("$.phone",is("076000000")))
		 		.andExpect(jsonPath("$.email",is("antonio@yahoo.com")))		
		 		.andExpect(jsonPath("$.name",is("Antonio")))
		 		.andExpect(jsonPath("$.surname",is("Todorov")))
		 		.andExpect(jsonPath("$.embg",is("987456321")))
		 		.andExpect(jsonPath("$.dateOfBirth",is(new Date("03/19/1991").getTime())))	 		
		 		.andExpect(jsonPath("$.nationality",is("Macedonian")))
		 		.andExpect(jsonPath("$.gender",is("MALE")))
		 		.andExpect(jsonPath("$.citizenship",is("Macedonian")))
		 		.andExpect(jsonPath("$.placeOfBirth",is("Kavadarci")))
		 		.andExpect(jsonPath("$.municipalityOfBirth",is("Kavadarci")))
		 		.andExpect(jsonPath("$.countryOfBirth",is("Macedonia")));
		 		
		 

	}
    
    @Test
	public void findByIdTest_NotSuccessfull_Student_NotFound() throws Exception{

		
		mockMvc.perform(get("/student/find/{id}",10L))
				.andExpect(status().isNotFound());

		
	}
    
    @Test
	public void addStudentTest_NotSuccessfull() throws Exception{
			
		// First student
			Student student1=new Student();
				
				
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("078000000");
				
				/// EMAIL IS NULL
				student1.setEmail(null);
				
				// Personal info
				student1.setName(null);
				student1.setSurname("Mitrev");
				student1.setEMBG("123456987");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				
				mockMvc.perform(post("/student/addStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student is not added successfully")));

	}
    @Test
	public void addStudentTest_Successfull() throws Exception{
			
		// First student
				Student student1=new Student();
				
				
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("072000000");
				student1.setEmail("Mitre@yahoo.com");
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				student1.setEMBG("963852741");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				
				
				mockMvc.perform(post("/student/addStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student successfully added")));		
		
	}	
    @Test
	public void updateStudentTest_Successfull() throws Exception{
			
		// First student
				Student student1=new Student();
				
				student1.setId(2L);
				student1.setIndex(131072);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.STATE);
				student1.setStudiesProgram("KNI");
		
				// Changing place of living
				student1.setPlaceOfLiving("Bitola");
				
				// Changing municipality of living
				student1.setMunicipalityOfLiving("Bitola");
		
				student1.setCountry("Macedonia");
				
				// Changing address of living
				student1.setAddress("Bitolska");
				
				student1.setPhone("078000000");
				student1.setEmail("ilija@yahoo.com");
				
				// Personal info
				student1.setName("Ilija");
				student1.setSurname("Ilijev");
				student1.setEMBG("123456789");
				student1.setDateOfBirth(new Date("09/05/2016"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Kavadarci");
				student1.setMunicipalityOfBirth("Kavadarci");
				student1.setCountryOfBirth("Macedonia");
			
				mockMvc.perform(post("/student/updateStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student successfully updated")));
		
	}	
	
	@Test
	public void updateStudentTest_NotSuccessfull() throws Exception{
			
		// First student
		Student student1=new Student();
		
				student1.setId(2L);
				student1.setIndex(131072);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.STATE);
				student1.setStudiesProgram("KNI");
		
				// Changing place of living
				student1.setPlaceOfLiving("");
				
				// Changing municipality of living
				student1.setMunicipalityOfLiving("");
		
				student1.setCountry("Macedonia");
				
				// Changing address of living
				student1.setAddress("Bitolska");
				
				student1.setPhone("078000000");
				student1.setEmail("ilija@yahoo.com");
				
				// Personal info
				student1.setName("Ilija");
				student1.setSurname("Ilijev");
				student1.setEMBG("123456789");
				student1.setDateOfBirth(new Date("09/05/2016"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Kavadarci");
				student1.setMunicipalityOfBirth("Kavadarci");
				student1.setCountryOfBirth("Macedonia");
				
				
				mockMvc.perform(post("/student/updateStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student is not successfully updated")));
	
	}	
	
	
	@Test
	public void deleteStudentTest_Successfull() throws Exception{
			
		
				
				mockMvc.perform(delete("/student/delete/{id}",3L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student successfully deleted")));
								
	}	
	
	
	@Test
	public void deleteStudentTest_NotSuccessfull() throws Exception{

				
				mockMvc.perform(delete("/student/delete/{id}",10L))
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student is not successfully deleted")));
			
	}	
	
	@Test
	public void deleteStudentCourseTest_Successfull() throws Exception{

				
				mockMvc.perform(delete("/student/deleteSC/{id}/{course_id}",2L,1L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student course data successfully deleted")));
					
				
				
	}	
	
	@Test
	public void deleteStudentCourseTest_NotSuccessfull() throws Exception{
		
		
		
		mockMvc.perform(delete("/student/deleteSC/{id}/{course_id}",10L,2L)
				
			)
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.status",is("err")))
			.andExpect(jsonPath("$.message",is("Student course data is not successfully deleted")));
			
		
		
      		
	}	
	
    
    
}
