package com.web.app.project.service.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import com.web.app.project.dao.IStudentDao;
import com.web.app.project.dao.StudentDaoImpl;
import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;

import com.web.app.project.model.Course;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;
import com.web.app.project.service.IStudentService;
import com.web.app.project.service.StudentServiceImpl;

import javax.persistence.NoResultException;



public class StudentServiceUnitTest {
	
	private IStudentService studentService;
	
	private IStudentDao studentDao;
	
	private Student student1;
	
	private Student student2;
	
	private StudentCourse studentCourse1;
		
	@Before
	public void setUp(){
		
		studentService=new StudentServiceImpl();
		student1=mock(Student.class);
		student2=mock(Student.class);
		
		studentCourse1=mock(StudentCourse.class);
		studentDao=mock(StudentDaoImpl.class);
		studentService.setStudentDao(studentDao);
	
	}
	
	@Test
	  public void testMockCreation(){
	        assertNotNull(student1);
	        assertNotNull(student2);
	        assertNotNull(studentCourse1);
	        assertNotNull(studentDao);	   
	    }
	
	@Test
	public void getAllStudentTest_Success_With2Students(){
		
		//student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		// Second student
		//	student2=new Student();
				student2.setId(2L);
				student2.setIndex(142123);
				student2.setYearOfEnrollment("2014");
				student2.setStatus(Status.REGULAR);
				student2.setCycle(StudiesCycle.FIRST);
				student2.setQuota(Quota.PRIVATE);
				student2.setStudiesProgram("KNI");
				student2.setPlaceOfLiving("Skopje");
				student2.setMunicipalityOfLiving("Karposh");
				student2.setCountry("Macedonia");
				student2.setAddress("Ruzveltova");
				student2.setPhone("076000000");
				student2.setEmail("Blashko@yahoo.com");
				
				// Personal info
				student2.setName("Blashko");
				student2.setSurname("Mitrev");
				student2.setEMBG("321654987");
				student2.setDateOfBirth(new Date("01/02/1995"));
				student2.setNationality("Macedonian");
				student2.setGender(Gender.MALE);
				student2.setCitizenship("Macedonian");
				student2.setPlaceOfBirth("Skopje");
				student2.setMunicipalityOfBirth("Skopje");
				student2.setCountryOfBirth("Macedonia");
				
				StudentCourse studentCourse1=new StudentCourse();
				ArrayList<StudentCourse> scList1=new ArrayList<StudentCourse>();
				scList1.add(studentCourse1);
				student2.setStudentCourse(scList1);
				
				
				when(studentDao.getAllStudents()).thenReturn(Arrays.asList(student1,student2));
				
				
				
				
				// Calling getAllStudents method
				studentService.getAllStudents();
				
				// verifying times of calling getAllStudents() method
				verify(studentDao,times(1)).getAllStudents();
				
			
				
				
				//Assertion 
				assertEquals(Arrays.asList(student1,student2),studentDao.getAllStudents());
			
	}
	
	@Test
	public void getAllStudentTest_Success_No_Students(){
		
				ArrayList<Student> students=new ArrayList<Student>();
				
				when(studentDao.getAllStudents()).thenReturn(students);
				
				studentService.getAllStudents();
				
				
				verify(studentDao,times(1)).getAllStudents();
			
				
				assertEquals(students,studentDao.getAllStudents());
	}
	
	@Test
	public void findById_Success() throws NoResultException{
		
		//student2=new Student();
		student2.setId(2L);
		student2.setIndex(142123);
		student2.setYearOfEnrollment("2014");
		student2.setStatus(Status.REGULAR);
		student2.setCycle(StudiesCycle.FIRST);
		student2.setQuota(Quota.PRIVATE);
		student2.setStudiesProgram("KNI");
		student2.setPlaceOfLiving("Skopje");
		student2.setMunicipalityOfLiving("Karposh");
		student2.setCountry("Macedonia");
		student2.setAddress("Ruzveltova");
		student2.setPhone("076000000");
		student2.setEmail("Blashko@yahoo.com");
		
		// Personal info
		student2.setName("Blashko");
		student2.setSurname("Mitrev");
		student2.setEMBG("321654987");
		student2.setDateOfBirth(new Date("01/02/1995"));
		student2.setNationality("Macedonian");
		student2.setGender(Gender.MALE);
		student2.setCitizenship("Macedonian");
		student2.setPlaceOfBirth("Skopje");
		student2.setMunicipalityOfBirth("Skopje");
		student2.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse1=new StudentCourse();
		ArrayList<StudentCourse> scList1=new ArrayList<StudentCourse>();
		scList1.add(studentCourse1);
		student2.setStudentCourse(scList1);
		
		
		
		when(studentDao.findById(2L)).thenReturn(student2);
		
		
		
		
		// Calling findById method
		studentService.findById(2L);
		
		// verifying times of calling findById() method
		verify(studentDao,times(1)).findById(2L);
		
		
		
		//Assertion 
		assertEquals(student2,studentDao.findById(2L));
	
		
	}
	
	@Test
	public void findById_NotFound(){
			
		when(studentDao.findById(1L)).thenReturn(null);
		
		// Calling findById method
		studentService.findById(1L);	
		
		// verifying times of calling findById() method
		verify(studentDao,times(1)).findById(1L);
		
		assertNull(studentDao.findById(1L));
	}
	
	@Test
	public void deleteStudent_Successfull()
	{
		HashMap<String,String> result=new HashMap<String,String>();
		result.put("status", "ok");
		result.put("message","Student successfuly deleted");
		
		
		when(studentDao.deleteStudent(2L)).thenReturn(result);

		
		// Calling deleteStudent method
		studentService.deleteStudent(2L);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).deleteStudent(2L);
			
		//Assertion 
		assertEquals(result,studentDao.deleteStudent(2L));
	
	}
	
	@Test
	public void deleteStudent_NotSuccessfull_NotFoundStudent()
	{
		
		
		HashMap<String,String> result=new HashMap<String,String>();
		result.put("status", "err");
		result.put("message","Student is not successfully deleted");
		
		
		when(studentDao.deleteStudent(2L)).thenReturn(result);
		

		// Calling deleteStudent method
		studentService.deleteStudent(2L);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).deleteStudent(2L);
		
		assertEquals(result,studentDao.deleteStudent(2L));
	
	}
	@Test
	public void addStudentTest_Successfull()
	{
		HashMap<String,String> result = new HashMap<String,String> ();

		result.put("status", "ok");
		result.put("message","Student successfuly added");
		
		//student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		when(studentDao.addStudent(student1)).thenReturn(result);
		
		
		// Calling deleteStudent method
		studentService.addStudent(student1);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).addStudent(student1);
		
		// Order of executing
		InOrder order=inOrder(studentDao);
		order.verify(studentDao).addStudent(student1);
		
		
		//Assertion 
		assertEquals(result,studentDao.addStudent(student1));
	}
	@Test
	public void addStudentTest_NotSuccessfull_Not_Null_property_is_null()
	{
		
		
		HashMap<String,String> result = new HashMap<String,String> ();
		
		result.put("status", "err");
		result.put("message","Student is not added successfuly");
		//student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		//student1.setPlaceOfLiving("Skopje");
		//student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		when(studentDao.addStudent(student1)).thenReturn(result);
		
		
		// Calling deleteStudent method
		studentService.addStudent(student1);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).addStudent(student1);
		
		// Order of executing
		InOrder order=inOrder(studentDao);
		order.verify(studentDao).addStudent(student1);
		
		//Assertion 
		assertEquals(result,studentDao.addStudent(student1));

	}
	
	
	@Test
	public void updateStudent_Successfull()
	{
		HashMap<String,String> result = new HashMap<String,String> ();

		result.put("status", "ok");
		result.put("message","Student successfuly updated");
		
		
		//student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		when(studentDao.updateStudent(student1)).thenReturn(result);
		
		
		// Calling deleteStudent method
		studentService.updateStudent(student1);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).updateStudent(student1);
		
		// Order of executing
		InOrder order=inOrder(studentDao);
		order.verify(studentDao).updateStudent(student1);
		
		
		//Assertion 
		assertEquals(result,studentDao.updateStudent(student1));
	
	}
	
	@Test
	public void updateStudent_NotSuccessfull()
	{
		HashMap<String,String> result = new HashMap<String,String> ();
		
		
		 result.put("status", "err");
         result.put("message","Student is not successfuly updated");
		
		//student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving(null);
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		when(studentDao.updateStudent(student1)).thenReturn(result);
		
		
		// Calling deleteStudent method
		studentService.updateStudent(student1);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).updateStudent(student1);
		
		// Order of executing
		InOrder order=inOrder(studentDao);
		order.verify(studentDao).updateStudent(student1);
		
		
		//Assertion 
		assertEquals(result,studentDao.updateStudent(student1));
	
	}
	
	@Test
	public void deleteStudentCourse_Successfull() throws NoResultException
	{
		
		
		HashMap<String,String> result=new HashMap<String,String>();
		result.put("status", "ok");
		result.put("message","Student successfuly deleted");
		
		
		when(studentDao.deleteStudentCourse(1L,2L)).thenReturn(result);

		
		// Calling deleteStudent method
		studentService.deleteStudentCourse(1L,2L);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).deleteStudentCourse(1L,2L);
		
		// Order of executing
		InOrder order=inOrder(studentDao);
		order.verify(studentDao).deleteStudentCourse(1L,2L);
		
		
		//Assertion 
		assertEquals(result,studentDao.deleteStudentCourse(1L,2L));
	
		
	}
	
	@Test
	public void deleteStudentCourse_NotSuccessfull() 
	{
		
		
		HashMap<String,String> result=new HashMap<String,String>();
		result.put("status", "err");
	    result.put("message","Course is not successfully deleted");
		
		when(studentDao.deleteStudentCourse(1L,2L)).thenReturn(result);

		
		// Calling deleteStudent method
		studentService.deleteStudentCourse(1L,2L);
		
		// verifying times of calling deleteStudent() method
		verify(studentDao,times(1)).deleteStudentCourse(1L,2L);
		
		// Order of executing
		InOrder order=inOrder(studentDao);
		order.verify(studentDao).deleteStudentCourse(1L,2L);
		
		
		//Assertion 
		assertEquals(result,studentDao.deleteStudentCourse(1L,2L));	
	}
	
	

}
