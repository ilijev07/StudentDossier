package com.web.app.project.controllers.test;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.web.app.project.controllers.CourseController;

import com.web.app.project.helper.TestUtil;
import com.web.app.project.model.Course;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;
import com.web.app.project.service.CourseServiceImpl;


import org.mockito.ArgumentCaptor;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import static org.junit.Assert.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class CourseControllerUnitTest {
	
	@Mock
	private CourseServiceImpl courseService;
	
	@Mock
	private Course course1;
	
	@Mock
	private Course course2;
	
	@Mock
	private Student student1;
	
	@Mock
	private StudentCourse studentCourse1;
	
	@Mock 
	private StudentCourse studentCourse2;
	
	@InjectMocks
	private CourseController courseController;
	
	private MockMvc mockMvc;
	
	@Before
	public void setUp(){
		 MockitoAnnotations.initMocks(this);
		 this.mockMvc = MockMvcBuilders.standaloneSetup(courseController).build();	 	
	}
	
	@Test
	public void getAllCoursesTest_Successfull_Two_Courses() throws Exception{
		
		// Course 1
		course1=new Course();
		
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setFund_classes("3+2+2");
		course1.setContentOfProgram("Content of program Calculus 1");
		course1.setGoalOfProgram("Goal of program Calculus 1");
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");
		
		// Course 2
		course2=new Course();
		
		course2.setCourseId(2L);
		course2.setCourseName("Calculus 2");
		course2.setFund_classes("3+2+1");
		course2.setContentOfProgram("Content of program Calculus 2");
		course2.setGoalOfProgram("Goal of program Calculus 2");
		course2.setLanguage("English");
		course2.setProfessor("Biljana Tojtovska");
		
		when(courseService.getAllCourses()).thenReturn(Arrays.asList(course1,course2));
		
		mockMvc.perform(get("/course/all"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$",hasSize(2)))
				.andExpect(jsonPath("$[0].courseId", is(1)))
				.andExpect(jsonPath("$[0].courseName", is("Calculus 1")))
				.andExpect(jsonPath("$[0].fund_classes", is("3+2+2")))
				.andExpect(jsonPath("$[0].contentOfProgram", is("Content of program Calculus 1")))
				.andExpect(jsonPath("$[0].goalOfProgram", is("Goal of program Calculus 1")))
				.andExpect(jsonPath("$[0].language", is("English")))
				.andExpect(jsonPath("$[0].professor", is("Verica Bakeva")))
				
				.andExpect(jsonPath("$[1].courseId", is(2)))
				.andExpect(jsonPath("$[1].courseName", is("Calculus 2")))
				.andExpect(jsonPath("$[1].fund_classes", is("3+2+1")))
				.andExpect(jsonPath("$[1].contentOfProgram", is("Content of program Calculus 2")))
				.andExpect(jsonPath("$[1].goalOfProgram", is("Goal of program Calculus 2")))
				.andExpect(jsonPath("$[1].language", is("English")))
				.andExpect(jsonPath("$[1].professor", is("Biljana Tojtovska")));
		
		
			verify(courseService,times(1)).getAllCourses();
		
			verifyNoMoreInteractions(courseService);
	
	}
	
	@Test
	public void getAllCoursesTest_Successfull_No_Courses() throws Exception{
		
		ArrayList<Course> courses=new ArrayList<Course>();
		when(courseService.getAllCourses()).thenReturn(courses);
		
		mockMvc.perform(get("/course/all"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$",hasSize(0)));
		
		
			verify(courseService,times(1)).getAllCourses();
		
			verifyNoMoreInteractions(courseService);
	
	}
	@Test
	public void findByIdTest_Successfull_Course_Found() throws Exception{
		
		// Course 1
		course1=new Course();
		
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setFund_classes("3+2+2");
		course1.setContentOfProgram("Content of program Calculus 1");
		course1.setGoalOfProgram("Goal of program Calculus 1");
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");
		
		
		
		when(courseService.findById(1L)).thenReturn(course1);
		
		mockMvc.perform(get("/course/find/{id}",1L))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.courseId", is(1)))
				.andExpect(jsonPath("$.courseName", is("Calculus 1")))
				.andExpect(jsonPath("$.fund_classes", is("3+2+2")))
				.andExpect(jsonPath("$.contentOfProgram", is("Content of program Calculus 1")))
				.andExpect(jsonPath("$.goalOfProgram", is("Goal of program Calculus 1")))
				.andExpect(jsonPath("$.language", is("English")))
				.andExpect(jsonPath("$.professor", is("Verica Bakeva")));
		
		
			verify(courseService,times(1)).findById(1L);
		
			verifyNoMoreInteractions(courseService);
	
	}
	
	@Test
	public void findByIdTest_NotSuccessfull_Course_NotFound() throws Exception{
			
		when(courseService.findById(10L)).thenReturn(null);
		
		mockMvc.perform(get("/course/find/{id}",10L))
				.andExpect(status().isNotFound());
				
		
		
			verify(courseService,times(1)).findById(10L);
		
			verifyNoMoreInteractions(courseService);
	}
	
	@Test
	public void addCourseTest_Successfull() throws Exception{
			
		// Course 2 to ADD
				course2=new Course();
				course2.setCourseId(1L);
				course2.setCourseName("Calculus 1");
				course2.setFund_classes("3+2+2");
				course2.setContentOfProgram("Content of program Calculus 1");
				course2.setGoalOfProgram("Goal of program Calculus 1");
				course2.setLanguage("English");
				course2.setProfessor("Verica Bakeva");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "ok");
				result.put("message","Course successfully added");
				
				when(courseService.addCourse(any(Course.class))).thenReturn(result);
				
				mockMvc.perform(post("/course/addCourse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(course2))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Course successfully added")));
					
				
				ArgumentCaptor<Course> courseCaptor = ArgumentCaptor.forClass(Course.class);
		        verify(courseService, times(1)).addCourse(courseCaptor.capture());
		        verifyNoMoreInteractions(courseService);
				
				Course courseArgument = courseCaptor.getValue();
				assertThat(courseArgument.getCourseId(), is(1L));
				assertThat(courseArgument.getContentOfProgram(),is("Content of program Calculus 1"));
				assertThat(courseArgument.getCourseName(),is("Calculus 1"));
				assertThat(courseArgument.getFund_classes(),is("3+2+2"));
				assertThat(courseArgument.getGoalOfProgram(),is("Goal of program Calculus 1"));
				assertThat(courseArgument.getLanguage(),is("English"));
				assertThat(courseArgument.getProfessor(),is("Verica Bakeva"));
				
	}	
	
	
	@Test
	public void addCourseTest_NotSuccessfull() throws Exception{
			
		// Course 2 to ADD
				course2=new Course();
				course2.setCourseId(1L);
				course2.setCourseName(null);
				course2.setFund_classes("3+2+2");
				course2.setContentOfProgram("Content of program Calculus 1");
				course2.setGoalOfProgram("Goal of program Calculus 1");
				course2.setLanguage("English");
				course2.setProfessor("Verica Bakeva");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "err");
				result.put("message","Course is not successfully added");
				
				when(courseService.addCourse(any(Course.class))).thenReturn(result);
				
				mockMvc.perform(post("/course/addCourse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(course2))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Course is not successfully added")));
					
				
				ArgumentCaptor<Course> courseCaptor = ArgumentCaptor.forClass(Course.class);
		        verify(courseService, times(1)).addCourse(courseCaptor.capture());
		        verifyNoMoreInteractions(courseService);
				
				Course courseArgument = courseCaptor.getValue();
				assertThat(courseArgument.getCourseId(), is(1L));
				assertThat(courseArgument.getContentOfProgram(),is("Content of program Calculus 1"));
				// Assert NULL for courseName
				assertNull(courseArgument.getCourseName());
				assertThat(courseArgument.getFund_classes(),is("3+2+2"));
				assertThat(courseArgument.getGoalOfProgram(),is("Goal of program Calculus 1"));
				assertThat(courseArgument.getLanguage(),is("English"));
				assertThat(courseArgument.getProfessor(),is("Verica Bakeva"));
				
	}	
	
	@Test
	public void updateCourseTest_Successfull() throws Exception{
			
		// Course 2 to ADD
				course2=new Course();
				course2.setCourseId(1L);
				course2.setCourseName("Calculus 2");
				course2.setFund_classes("3+2+2");
				course2.setContentOfProgram("Content of program Calculus 1");
				course2.setGoalOfProgram("Goal of program Calculus 1");
				course2.setLanguage("English");
				course2.setProfessor("Verica Bakeva");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				 result.put("status", "ok");
			        result.put("message","Course successfully updated");
				
				when(courseService.updateCourse(any(Course.class))).thenReturn(result);
				
				mockMvc.perform(post("/course/updateCourse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(course2))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Course successfully updated")));
					
				
				ArgumentCaptor<Course> courseCaptor = ArgumentCaptor.forClass(Course.class);
		        verify(courseService, times(1)).updateCourse(courseCaptor.capture());
		        verifyNoMoreInteractions(courseService);
				
				Course courseArgument = courseCaptor.getValue();
				assertThat(courseArgument.getCourseId(), is(1L));
				assertThat(courseArgument.getContentOfProgram(),is("Content of program Calculus 1"));
				
				assertThat(courseArgument.getCourseName(),is("Calculus 2"));
			
				assertThat(courseArgument.getFund_classes(),is("3+2+2"));
				assertThat(courseArgument.getGoalOfProgram(),is("Goal of program Calculus 1"));
				assertThat(courseArgument.getLanguage(),is("English"));
				assertThat(courseArgument.getProfessor(),is("Verica Bakeva"));
				
	}	
	
	
	@Test
	public void updateCourseTest_NotSuccessfull() throws Exception{
			
		// Course 2 to ADD
				course2=new Course();
				course2.setCourseId(1L);
				course2.setCourseName("Calculus 2");
				course2.setFund_classes("3+2+2");
				course2.setContentOfProgram("Content of program Calculus 1");
				course2.setGoalOfProgram("Goal of program Calculus 1");
				course2.setLanguage(null);
				course2.setProfessor("Verica Bakeva");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "err");
		        result.put("message","Course is not successfully updated");
				
				when(courseService.updateCourse(any(Course.class))).thenReturn(result);
				
				mockMvc.perform(post("/course/updateCourse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(course2))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Course is not successfully updated")));
					
				
				ArgumentCaptor<Course> courseCaptor = ArgumentCaptor.forClass(Course.class);
		        verify(courseService, times(1)).updateCourse(courseCaptor.capture());
		        verifyNoMoreInteractions(courseService);
				
				Course courseArgument = courseCaptor.getValue();
				assertThat(courseArgument.getCourseId(), is(1L));
				assertThat(courseArgument.getContentOfProgram(),is("Content of program Calculus 1"));
				
				assertThat(courseArgument.getCourseName(),is("Calculus 2"));
			
				assertThat(courseArgument.getFund_classes(),is("3+2+2"));
				assertThat(courseArgument.getGoalOfProgram(),is("Goal of program Calculus 1"));
				// AssertNull LANGUAGE, updateFails
				assertNull(courseArgument.getLanguage());
				assertThat(courseArgument.getProfessor(),is("Verica Bakeva"));
				
	}	
	
	
	@Test
	public void deleteCourseTest_Successfull() throws Exception{
			
		
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "ok");
				result.put("message","Course successfully deleted");
				
				when(courseService.deleteCourse(1L)).thenReturn(result);
				
				mockMvc.perform(delete("/course/delete/{id}",1L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Course successfully deleted")));
					
				
				
		        verify(courseService, times(1)).deleteCourse(1L);
		        verifyNoMoreInteractions(courseService);				
	}	
	
	@Test
	public void deleteCourseTest_NotSuccessfull() throws Exception{
			
		
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "err");
			    result.put("message","Course is not successfully deleted");
				
				when(courseService.deleteCourse(1L)).thenReturn(result);
				
				mockMvc.perform(delete("/course/delete/{id}",1L))
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Course is not successfully deleted")));
					
				
				
		        verify(courseService, times(1)).deleteCourse(1L);
		        verifyNoMoreInteractions(courseService);				
	}	
	/*@Test
	public void addStudentCourseTest_Successfull() throws Exception{

			student1.setId(1L);
			student1.setIndex(131000);
			student1.setYearOfEnrollment("2013");
			student1.setStatus(Status.REGULAR);
			student1.setCycle(StudiesCycle.FIRST);
			student1.setQuota(Quota.PRIVATE);
			student1.setStudiesProgram("KNI");
			student1.setPlaceOfLiving("Skopje");
			student1.setMunicipalityOfLiving("Karposh");
			student1.setCountry("Macedonia");
			student1.setAddress("Bihakjka");
			student1.setPhone("078000000");
			student1.setEmail("Mitre@yahoo.com");
			
			// Personal info
			student1.setName("Mitre");
			student1.setSurname("Mitrev");
			student1.setEMBG("123456987");
			student1.setDateOfBirth(new Date("01/02/1994"));
			student1.setNationality("Macedonian");
			student1.setGender(Gender.MALE);
			student1.setCitizenship("Macedonian");
			student1.setPlaceOfBirth("Skopje");
			student1.setMunicipalityOfBirth("Skopje");
			student1.setCountryOfBirth("Macedonia");
				
				
				// Course 1
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setContentOfProgram("Content of program for Calculus 1");
				course1.setFund_classes("3+2+1");
				course1.setGoalOfProgram("Goal of program for Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
			// StudentCourse
				
				studentCourse1.setCourse(course1);
				studentCourse1.setStudent(student1);
				studentCourse1.setGrade(10.00);
				studentCourse1.setSchoolYear(2013);
				studentCourse1.setSemester("WINTER");
				
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "ok");
				result.put("message","Student course data successfully added");
				
				when(courseService.addStudentData(any(StudentCourse.class))).thenReturn(result);
				
				mockMvc.perform(post("/course/addStudentCourse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(studentCourse1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student course data successfully added")));
					
				
				ArgumentCaptor<StudentCourse> courseCaptor = ArgumentCaptor.forClass(StudentCourse.class);
		        verify(courseService, times(1)).addStudentData(courseCaptor.capture());
		        verifyNoMoreInteractions(courseService);
				
				StudentCourse courseArgument = courseCaptor.getValue();
				assertThat(courseArgument.getStudent(), is(student1));
				assertThat(courseArgument.getCourse(),is(course1));
				assertThat(courseArgument.getGrade(),is(10.00));
				assertThat(courseArgument.getSchoolYear(),is(2013));
				assertThat(courseArgument.getSemester(),is("WINTER"));
				
				
	}	
	
	@Test
	public void addStudentCourseTest_NotSuccessfull() throws Exception{

			
				
				
				// Course 1
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setContentOfProgram("Content of program for Calculus 1");
				course1.setFund_classes("3+2+1");
				course1.setGoalOfProgram("Goal of program for Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
			// StudentCourse
				
				studentCourse1.setCourse(course1);
				studentCourse1.setStudent(null);
				studentCourse1.setGrade(10.00);
				studentCourse1.setSchoolYear(2013);
				studentCourse1.setSemester("WINTER");
				
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "err");
				result.put("message","Student course data is not successfully added");
				when(courseService.addStudentData(any(StudentCourse.class))).thenReturn(result);
				
				mockMvc.perform(post("/course/addStudentCourse")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(studentCourse1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student course data is not successfully added")));
					
				
				ArgumentCaptor<StudentCourse> courseCaptor = ArgumentCaptor.forClass(StudentCourse.class);
		        verify(courseService, times(1)).addStudentData(courseCaptor.capture());
		        verifyNoMoreInteractions(courseService);
				
				StudentCourse courseArgument = courseCaptor.getValue();
				assertThat(courseArgument.getStudent(), is(student1));
				// Assertion null for course
				assertNull(courseArgument.getCourse());
				assertThat(courseArgument.getGrade(),is(10.00));
				assertThat(courseArgument.getSchoolYear(),is(2013));
				assertThat(courseArgument.getSemester(),is("WINTER"));
				
				
	}	
	*/
	
/*	
	@Test 
	public void getAllStudentCoursesTest_Successfull() throws Exception{
		// Student 1
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
			
		
		// Course 1
				course1=new Course();
				
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setFund_classes("3+2+2");
				course1.setContentOfProgram("Content of program Calculus 1");
				course1.setGoalOfProgram("Goal of program Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
				// Course 2
				course2=new Course();
				
				course2.setCourseId(2L);
				course2.setCourseName("Calculus 2");
				course2.setFund_classes("3+2+1");
				course2.setContentOfProgram("Content of program Calculus 2");
				course2.setGoalOfProgram("Goal of program Calculus 2");
				course2.setLanguage("English");
				course2.setProfessor("Biljana Tojtovska");
				
				
				
				// StudentCourse 1
					
					studentCourse1.setCourse(course1);
					studentCourse1.setStudent(student1);
					studentCourse1.setGrade(10.00);
					studentCourse1.setSchoolYear(2013);
					studentCourse1.setSemester("WINTER");
				
					
					
					// StudentCourse 1
						
						studentCourse2.setCourse(course2);
						studentCourse2.setStudent(student1);
						studentCourse2.setGrade(7.00);
						studentCourse2.setSchoolYear(2015);
						studentCourse2.setSemester("SUMMER");	
						
						
				when(courseService.getAllStudentCourses(1L)).thenReturn(Arrays.asList(studentCourse1,studentCourse2));
				
				mockMvc.perform(get("/course/allStudentCourses/{id}",1L))
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$",hasSize(2)))
						.andExpect(jsonPath("$[0].student", is(student1)))
						.andExpect(jsonPath("$[0].course", is(course1)))
						.andExpect(jsonPath("$[0].grade", is(10.00)))
						.andExpect(jsonPath("$[0].schoolYear", is(2013)))
						.andExpect(jsonPath("$[0].semester", is("WINTER")))
						
						
						.andExpect(jsonPath("$[1].student", is(student1)))
						.andExpect(jsonPath("$[1].course", is(course2)))
						.andExpect(jsonPath("$[1].course", is(7.00)))
						.andExpect(jsonPath("$[1].schoolYear", is(2015)))
						.andExpect(jsonPath("$[1].semester", is("SUMMER")));
				
				
					verify(courseService,times(1)).getAllStudentCourses(1L);
				
					verifyNoMoreInteractions(courseService);
	}
	*/
	@Test 
	public void getAllStudentCoursesTest_Successfull_NumOfStudentCoursesIsZero() throws Exception{
				
				ArrayList<StudentCourse> studentCourses=new ArrayList<StudentCourse>();
						
				when(courseService.getAllStudentCourses(1L)).thenReturn(studentCourses);
				
				mockMvc.perform(get("/course/allStudentCourses/{id}",1L))
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$",hasSize(0)));
							
				
					verify(courseService,times(1)).getAllStudentCourses(1L);
				
					verifyNoMoreInteractions(courseService);
	}
}
