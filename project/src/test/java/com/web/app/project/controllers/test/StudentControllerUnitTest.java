package com.web.app.project.controllers.test;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.web.app.project.controllers.StudentController;
import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;
import com.web.app.project.helper.TestUtil;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;
import com.web.app.project.service.StudentServiceImpl;


import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



import antlr.collections.List;



@RunWith(MockitoJUnitRunner.class)
public class StudentControllerUnitTest {
	
	@Mock
	private StudentServiceImpl studentService;
	
	@Mock
	private Student student1;
	
	@Mock
	private Student student2;
	
	
	@InjectMocks
	private StudentController studentController;
	
	
	private MockMvc mockMvc;
	
	@Before
	public void setUp(){
		 MockitoAnnotations.initMocks(this);
	
		 this.mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();	 
		 
	}
	
	@Test
	public void findAllStudents_Successfull_OneStudentReturned() throws Exception{
		
		// First student
		student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		
		 when(studentService.getAllStudents()).thenReturn(Arrays.asList(student1));
		
		 mockMvc.perform(get("/student/all"))
		 		.andExpect(status().isOk())
		 		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		 		.andExpect(jsonPath("$",hasSize(1)))
		 		.andExpect(jsonPath("$[0].id",is(1)))	
		 		.andExpect(jsonPath("$[0].index",is(131000)))
		 		.andExpect(jsonPath("$[0].yearOfEnrollment",is("2013")))
		 		.andExpect(jsonPath("$[0].status",is("REGULAR")))
		 		.andExpect(jsonPath("$[0].cycle",is("FIRST")))
		 		.andExpect(jsonPath("$[0].quota",is("PRIVATE")))
		 		.andExpect(jsonPath("$[0].studiesProgram",is("KNI")))
		 		.andExpect(jsonPath("$[0].placeOfLiving",is("Skopje")))
		 		.andExpect(jsonPath("$[0].municipalityOfLiving",is("Karposh")))
		 		.andExpect(jsonPath("$[0].country",is("Macedonia")))
		 		.andExpect(jsonPath("$[0].address",is("Bihakjka")))
		 		
		 		.andExpect(jsonPath("$[0].phone",is("078000000")))
		 		.andExpect(jsonPath("$[0].email",is("Mitre@yahoo.com")))
		 		
		 		.andExpect(jsonPath("$[0].name",is("Mitre")))
		 		.andExpect(jsonPath("$[0].surname",is("Mitrev")))
		 		.andExpect(jsonPath("$[0].embg",is("123456987")))
		 		.andExpect(jsonPath("$[0].dateOfBirth",is(new Date("01/02/1994").getTime())))
		 		
		 		.andExpect(jsonPath("$[0].nationality",is("Macedonian")))
		 		.andExpect(jsonPath("$[0].gender",is("MALE")))
		 		.andExpect(jsonPath("$[0].citizenship",is("Macedonian")))
		 		.andExpect(jsonPath("$[0].placeOfBirth",is("Skopje")))
		 		.andExpect(jsonPath("$[0].municipalityOfBirth",is("Skopje")))
		 		.andExpect(jsonPath("$[0].countryOfBirth",is("Macedonia")));
		 		
		 
		 verify(studentService,times(1)).getAllStudents();
	       verifyNoMoreInteractions(studentService);

		
		
	}
	
	@Test
	public void findAllStudents_Successfull_NoStudentReturned() throws Exception{
		
	
	
			ArrayList<Student> students=new ArrayList<Student>();
		
		 when(studentService.getAllStudents()).thenReturn(students);
		
		 mockMvc.perform(get("/student/all"))
		 		.andExpect(status().isOk())
		 		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		 		.andExpect(jsonPath("$",hasSize(0)));
		 		
		 		
		 
		 verify(studentService,times(1)).getAllStudents();
	       verifyNoMoreInteractions(studentService);
	}
	
	@Test
	public void findByIdTest_Successfull() throws Exception{
		
		// First student
		student1=new Student();
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
		
		StudentCourse studentCourse=new StudentCourse();
		ArrayList<StudentCourse> scList=new ArrayList<StudentCourse>();
		scList.add(studentCourse);
		student1.setStudentCourse(scList);
		
		
		
		 when(studentService.findById(1L)).thenReturn(student1);
		
		 mockMvc.perform(get("/student//find/{id}",1L))
		 		.andExpect(status().isOk())
		 		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		 		.andExpect(jsonPath("$.id",is(1)))	
		 		.andExpect(jsonPath("$.index",is(131000)))
		 		.andExpect(jsonPath("$.yearOfEnrollment",is("2013")))
		 		.andExpect(jsonPath("$.status",is("REGULAR")))
		 		.andExpect(jsonPath("$.cycle",is("FIRST")))
		 		.andExpect(jsonPath("$.quota",is("PRIVATE")))
		 		.andExpect(jsonPath("$.studiesProgram",is("KNI")))
		 		.andExpect(jsonPath("$.placeOfLiving",is("Skopje")))
		 		.andExpect(jsonPath("$.municipalityOfLiving",is("Karposh")))
		 		.andExpect(jsonPath("$.country",is("Macedonia")))
		 		.andExpect(jsonPath("$.address",is("Bihakjka")))
		 		
		 		.andExpect(jsonPath("$.phone",is("078000000")))
		 		.andExpect(jsonPath("$.email",is("Mitre@yahoo.com")))
		 		
		 		.andExpect(jsonPath("$.name",is("Mitre")))
		 		.andExpect(jsonPath("$.surname",is("Mitrev")))
		 		.andExpect(jsonPath("$.embg",is("123456987")))
		 		.andExpect(jsonPath("$.dateOfBirth",is(new Date("01/02/1994").getTime())))
		 		
		 		.andExpect(jsonPath("$.nationality",is("Macedonian")))
		 		.andExpect(jsonPath("$.gender",is("MALE")))
		 		.andExpect(jsonPath("$.citizenship",is("Macedonian")))
		 		.andExpect(jsonPath("$.placeOfBirth",is("Skopje")))
		 		.andExpect(jsonPath("$.municipalityOfBirth",is("Skopje")))
		 		.andExpect(jsonPath("$.countryOfBirth",is("Macedonia")));
		 		
		 
		 	verify(studentService,times(1)).findById(1L);
	       verifyNoMoreInteractions(studentService);

	}
	
	@Test
	public void findByIdTest_NotSuccessfull_Student_NotFound() throws Exception{
			
		when(studentService.findById(10L)).thenReturn(null);
		
		mockMvc.perform(get("/student/find/{id}",10L))
				.andExpect(status().isNotFound());
				
		
		
			verify(studentService,times(1)).findById(10L);
		
			verifyNoMoreInteractions(studentService);
	}
	
	
	@Test
	public void addStudentTest_NotSuccessfull() throws Exception{
			
		// First student
				student1=new Student();
				
				student1.setId(1L);
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("078000000");
				
				/// EMAIL IS NULL
				student1.setEmail(null);
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				student1.setEMBG("123456987");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "err");
				result.put("message","Student is not added successfully");
				
				when(studentService.addStudent(any(Student.class))).thenReturn(result);
				
				mockMvc.perform(post("/student/addStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student is not added successfully")));
					
				
				ArgumentCaptor<Student> studentCaptor = ArgumentCaptor.forClass(Student.class);
		        verify(studentService, times(1)).addStudent(studentCaptor.capture());
		        verifyNoMoreInteractions(studentService);
				
				Student studentArgument = studentCaptor.getValue();
				assertThat(studentArgument.getId(), is(1L));
				assertThat(studentArgument.getIndex(),is(131000));
				assertThat(studentArgument.getYearOfEnrollment(),is("2013"));
				assertThat(studentArgument.getStatus(),is(Status.REGULAR));
				assertThat(studentArgument.getCycle(),is(StudiesCycle.FIRST));
				assertThat(studentArgument.getQuota(),is(Quota.PRIVATE));
				assertThat(studentArgument.getStudiesProgram(),is("KNI"));
				
				assertThat(studentArgument.getPlaceOfLiving(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfLiving(),is("Karposh"));
				assertThat(studentArgument.getCountry(),is("Macedonia"));
				assertThat(studentArgument.getAddress(),is("Bihakjka"));
				assertThat(studentArgument.getPhone(),is("078000000"));
				// ASSERT NULL
				assertNull(studentArgument.getEmail());
				assertThat(studentArgument.getName(),is("Mitre"));
				assertThat(studentArgument.getSurname(),is("Mitrev"));
				assertThat(studentArgument.getEMBG(),is("123456987"));
				assertThat(studentArgument.getDateOfBirth(),is(new Date("01/02/1994")));
				assertThat(studentArgument.getNationality(),is("Macedonian"));
				assertThat(studentArgument.getGender(),is(Gender.MALE));
				assertThat(studentArgument.getCitizenship(),is("Macedonian"));
				assertThat(studentArgument.getPlaceOfBirth(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfBirth(),is("Skopje"));
				assertThat(studentArgument.getCountryOfBirth(),is("Macedonia"));
		
	}
	
	@Test
	public void addStudentTest_Successfull() throws Exception{
			
		// First student
				student1=new Student();
				
				student1.setId(1L);
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("078000000");
				student1.setEmail("Mitre@yahoo.com");
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				student1.setEMBG("123456987");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "ok");
				result.put("message","Student successfully added");
				
				when(studentService.addStudent(any(Student.class))).thenReturn(result);
				
				mockMvc.perform(post("/student/addStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student successfully added")));
					
				
				ArgumentCaptor<Student> studentCaptor = ArgumentCaptor.forClass(Student.class);
		        verify(studentService, times(1)).addStudent(studentCaptor.capture());
		        verifyNoMoreInteractions(studentService);
				
				Student studentArgument = studentCaptor.getValue();
				assertThat(studentArgument.getId(), is(1L));
				assertThat(studentArgument.getIndex(),is(131000));
				assertThat(studentArgument.getYearOfEnrollment(),is("2013"));
				assertThat(studentArgument.getStatus(),is(Status.REGULAR));
				assertThat(studentArgument.getCycle(),is(StudiesCycle.FIRST));
				assertThat(studentArgument.getQuota(),is(Quota.PRIVATE));
				assertThat(studentArgument.getStudiesProgram(),is("KNI"));
				
				assertThat(studentArgument.getPlaceOfLiving(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfLiving(),is("Karposh"));
				assertThat(studentArgument.getCountry(),is("Macedonia"));
				assertThat(studentArgument.getAddress(),is("Bihakjka"));
				assertThat(studentArgument.getPhone(),is("078000000"));
				assertThat(studentArgument.getEmail(),is("Mitre@yahoo.com"));
				assertThat(studentArgument.getName(),is("Mitre"));
				assertThat(studentArgument.getSurname(),is("Mitrev"));
				assertThat(studentArgument.getEMBG(),is("123456987"));
				assertThat(studentArgument.getDateOfBirth(),is(new Date("01/02/1994")));
				assertThat(studentArgument.getNationality(),is("Macedonian"));
				assertThat(studentArgument.getGender(),is(Gender.MALE));
				assertThat(studentArgument.getCitizenship(),is("Macedonian"));
				assertThat(studentArgument.getPlaceOfBirth(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfBirth(),is("Skopje"));
				assertThat(studentArgument.getCountryOfBirth(),is("Macedonia"));
		
	}	
	
	
	@Test
	public void updateStudentTest_Successfull() throws Exception{
			
		// First student
				student1=new Student();
				
				student1.setId(1L);
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("078000000");
				student1.setEmail("Mitre@yahoo.com");
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				student1.setEMBG("123456987");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				HashMap<String,String> result = new HashMap<String,String> ();
			    
                result.put("status", "ok");
                result.put("message","Student successfully updated");
				
				when(studentService.updateStudent(any(Student.class))).thenReturn(result);
				
				mockMvc.perform(post("/student/updateStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student successfully updated")));
					
				
				ArgumentCaptor<Student> studentCaptor = ArgumentCaptor.forClass(Student.class);
		        verify(studentService, times(1)).updateStudent(studentCaptor.capture());
		        verifyNoMoreInteractions(studentService);
				
				Student studentArgument = studentCaptor.getValue();
				assertThat(studentArgument.getId(), is(1L));
				assertThat(studentArgument.getIndex(),is(131000));
				assertThat(studentArgument.getYearOfEnrollment(),is("2013"));
				assertThat(studentArgument.getStatus(),is(Status.REGULAR));
				assertThat(studentArgument.getCycle(),is(StudiesCycle.FIRST));
				assertThat(studentArgument.getQuota(),is(Quota.PRIVATE));
				assertThat(studentArgument.getStudiesProgram(),is("KNI"));
				
				assertThat(studentArgument.getPlaceOfLiving(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfLiving(),is("Karposh"));
				assertThat(studentArgument.getCountry(),is("Macedonia"));
				assertThat(studentArgument.getAddress(),is("Bihakjka"));
				assertThat(studentArgument.getPhone(),is("078000000"));
				assertThat(studentArgument.getEmail(),is("Mitre@yahoo.com"));
				assertThat(studentArgument.getName(),is("Mitre"));
				assertThat(studentArgument.getSurname(),is("Mitrev"));
				assertThat(studentArgument.getEMBG(),is("123456987"));
				assertThat(studentArgument.getDateOfBirth(),is(new Date("01/02/1994")));
				assertThat(studentArgument.getNationality(),is("Macedonian"));
				assertThat(studentArgument.getGender(),is(Gender.MALE));
				assertThat(studentArgument.getCitizenship(),is("Macedonian"));
				assertThat(studentArgument.getPlaceOfBirth(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfBirth(),is("Skopje"));
				assertThat(studentArgument.getCountryOfBirth(),is("Macedonia"));
		
	}	
	
	
	
	@Test
	public void updateStudentTest_NotSuccessfull() throws Exception{
			
		// First student
				student1=new Student();
				
				student1.setId(1L);
				student1.setIndex(131000);
				student1.setYearOfEnrollment("2013");
				student1.setStatus(Status.REGULAR);
				student1.setCycle(StudiesCycle.FIRST);
				student1.setQuota(Quota.PRIVATE);
				student1.setStudiesProgram("KNI");
				student1.setPlaceOfLiving("Skopje");
				student1.setMunicipalityOfLiving("Karposh");
				student1.setCountry("Macedonia");
				student1.setAddress("Bihakjka");
				student1.setPhone("078000000");
				student1.setEmail("Mitre@yahoo.com");
				
				// Personal info
				student1.setName("Mitre");
				student1.setSurname("Mitrev");
				
				// EMBG EMPTY 
				student1.setEMBG("");
				student1.setDateOfBirth(new Date("01/02/1994"));
				student1.setNationality("Macedonian");
				student1.setGender(Gender.MALE);
				student1.setCitizenship("Macedonian");
				student1.setPlaceOfBirth("Skopje");
				student1.setMunicipalityOfBirth("Skopje");
				student1.setCountryOfBirth("Macedonia");
				
				HashMap<String,String> result = new HashMap<String,String> ();
			    
				result.put("status", "err");
                result.put("message","Student is not successfully updated");
				
				when(studentService.updateStudent(any(Student.class))).thenReturn(result);
				
				mockMvc.perform(post("/student/updateStudent")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TestUtil.convertObjectToJsonBytes(student1))
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student is not successfully updated")));
					
				
				ArgumentCaptor<Student> studentCaptor = ArgumentCaptor.forClass(Student.class);
		        verify(studentService, times(1)).updateStudent(studentCaptor.capture());
		        verifyNoMoreInteractions(studentService);
				
				Student studentArgument = studentCaptor.getValue();
				assertThat(studentArgument.getId(), is(1L));
				assertThat(studentArgument.getIndex(),is(131000));
				assertThat(studentArgument.getYearOfEnrollment(),is("2013"));
				assertThat(studentArgument.getStatus(),is(Status.REGULAR));
				assertThat(studentArgument.getCycle(),is(StudiesCycle.FIRST));
				assertThat(studentArgument.getQuota(),is(Quota.PRIVATE));
				assertThat(studentArgument.getStudiesProgram(),is("KNI"));
				
				assertThat(studentArgument.getPlaceOfLiving(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfLiving(),is("Karposh"));
				assertThat(studentArgument.getCountry(),is("Macedonia"));
				assertThat(studentArgument.getAddress(),is("Bihakjka"));
				assertThat(studentArgument.getPhone(),is("078000000"));
				assertThat(studentArgument.getEmail(),is("Mitre@yahoo.com"));
				assertThat(studentArgument.getName(),is("Mitre"));
				assertThat(studentArgument.getSurname(),is("Mitrev"));
				
				// EMBG EMPTY ASSERTION
				assertThat(studentArgument.getEMBG(),is(""));
				assertThat(studentArgument.getDateOfBirth(),is(new Date("01/02/1994")));
				assertThat(studentArgument.getNationality(),is("Macedonian"));
				assertThat(studentArgument.getGender(),is(Gender.MALE));
				assertThat(studentArgument.getCitizenship(),is("Macedonian"));
				assertThat(studentArgument.getPlaceOfBirth(),is("Skopje"));
				assertThat(studentArgument.getMunicipalityOfBirth(),is("Skopje"));
				assertThat(studentArgument.getCountryOfBirth(),is("Macedonia"));
		
	}	
	
	@Test
	public void deleteStudentTest_Successfull() throws Exception{
			
		
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "ok");
	    		result.put("message","Student successfully deleted");
				
				when(studentService.deleteStudent(1L)).thenReturn(result);
				
				mockMvc.perform(delete("/student/delete/{id}",1L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student successfully deleted")));
					
				
				
		        verify(studentService, times(1)).deleteStudent(1L);
		        verifyNoMoreInteractions(studentService);				
	}	
	
	
	@Test
	public void deleteStudentTest_NotSuccessfull() throws Exception{
			
		
				
				HashMap<String,String> result = new HashMap<String,String> ();
				result.put("status", "err");
				result.put("message","Student is not successfully deleted");
				
				when(studentService.deleteStudent(1L)).thenReturn(result);
				
				mockMvc.perform(delete("/student/delete/{id}",1L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student is not successfully deleted")));
					
				
				
		        verify(studentService, times(1)).deleteStudent(1L);
		        verifyNoMoreInteractions(studentService);				
	}	
	
	
	@Test
	public void deleteStudentCourseTest_Successfull() throws Exception{
			
		
				
				HashMap<String,String> result = new HashMap<String,String> ();
				 result.put("status", "ok");
			     result.put("message","Student course data successfully deleted");
				
				when(studentService.deleteStudentCourse(1L, 2L)).thenReturn(result);
				
				mockMvc.perform(delete("/student/deleteSC/{id}/{course_id}",1L,2L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("ok")))
					.andExpect(jsonPath("$.message",is("Student course data successfully deleted")));
					
				
				
		        verify(studentService, times(1)).deleteStudentCourse(1L, 2L);
		        verifyNoMoreInteractions(studentService);				
	}	
	
	
	
	@Test
	public void deleteStudentCourseTest_NotSuccessfull() throws Exception{
			
		
				
				HashMap<String,String> result = new HashMap<String,String> ();
				  result.put("status", "err");
			      result.put("message","Student course data is not successfully deleted");
				
				when(studentService.deleteStudentCourse(1L, 2L)).thenReturn(result);
				
				mockMvc.perform(delete("/student/deleteSC/{id}/{course_id}",1L,2L)
						
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("$.status",is("err")))
					.andExpect(jsonPath("$.message",is("Student course data is not successfully deleted")));
					
				
				
		        verify(studentService, times(1)).deleteStudentCourse(1L, 2L);
		        verifyNoMoreInteractions(studentService);				
	}	
}
