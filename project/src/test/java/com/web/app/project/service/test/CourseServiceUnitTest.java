package com.web.app.project.service.test;


import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.web.app.project.dao.CourseDaoImpl;
import com.web.app.project.dao.ICourseDao;
import com.web.app.project.enums.Gender;
import com.web.app.project.enums.Quota;
import com.web.app.project.enums.Status;
import com.web.app.project.enums.StudiesCycle;
import com.web.app.project.model.Course;
import com.web.app.project.model.Student;
import com.web.app.project.model.StudentCourse;
import com.web.app.project.service.CourseServiceImpl;
import com.web.app.project.service.ICourseService;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.NoResultException;


public class CourseServiceUnitTest {
	
	
	private ICourseService courseService;
	
	private ICourseDao courseDao;
	
	private Course course1;
	
	private Course course2;
	
	private Student student1;
	
	private Student student2;
	
	private StudentCourse studentCourse1;
	
	private StudentCourse studentCourse2;
	
	@Before
	public void setUp(){
		
		courseService=new CourseServiceImpl();
		courseDao=mock(CourseDaoImpl.class);
		courseService.setCourseDao(courseDao);
		
		course1=mock(Course.class);
		course2=mock(Course.class);
		
		student1=mock(Student.class);
		student2=mock(Student.class);
		
		studentCourse1=mock(StudentCourse.class);
		studentCourse2=mock(StudentCourse.class);
		
		
		
	}
	@Test
	public void testMockCreation(){
		assertNotNull(courseDao);
		assertNotNull(course1);
		assertNotNull(course2);
		
		assertNotNull(student1);
		assertNotNull(student2);
		
		assertNotNull(studentCourse1);
		assertNotNull(studentCourse2);
		
	}
	
	@Test
	public void getAllCoursesTest_Successfull_With2Courses(){
		
		// Course 1
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setContentOfProgram("Content of program for Calculus 1");
		course1.setFund_classes("3+2+1");
		course1.setGoalOfProgram("Goal of program for Calculus 1");
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");
		
		// Course 2
		
		course2.setCourseId(2L);
		course2.setCourseName("Calculus 2");
		course2.setContentOfProgram("Content of program for Calculus 2");
		course2.setFund_classes("3+2+1");
		course2.setGoalOfProgram("Goal of program for Calculus 2");
		course2.setLanguage("English");
		course2.setProfessor("Verica Bakeva");
		
		
		when(courseDao.getAllCourses()).thenReturn(Arrays.asList(course1,course2));
		
		
		
		
		// Calling getAllCourses method
		courseService.getAllCourses();
		
		// verifying times of calling getAllCourses() method
		verify(courseDao,times(1)).getAllCourses();
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).getAllCourses();
		
		
		//Assertion 
		assertEquals(Arrays.asList(course1,course2),courseDao.getAllCourses());
		
	}
	
	@Test
	public void getAllCoursesTest_Successfull_NoCourses(){
		
		ArrayList<Course> courses=new ArrayList<Course>();
		
		
		when(courseDao.getAllCourses()).thenReturn(courses);

		// Calling getAllCourses method
		courseService.getAllCourses();
		
		// verifying times of calling getAllCourses() method
		verify(courseDao,times(1)).getAllCourses();
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).getAllCourses();
		
		
		//Assertion 
		assertEquals(courses,courseDao.getAllCourses());
		
	}
	@Test
	public void findByIdTest_Successfull(){
		
		
		// Course 1
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setContentOfProgram("Content of program for Calculus 1");
				course1.setFund_classes("3+2+1");
				course1.setGoalOfProgram("Goal of program for Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
				
				
				when(courseDao.findById(1L)).thenReturn(course1);

				// Calling findById method
				courseService.findById(1L);
				
				// verifying times of calling findById method
				verify(courseDao,times(1)).findById(1L);
				
				// Order of executing
				InOrder order=inOrder(courseDao);
				order.verify(courseDao).findById(1L);
				
				
				//Assertion 
				assertEquals(course1,courseDao.findById(1L));	
	}
	
	@Test(expected=NoResultException.class)
	public void findById_NotFound(){
			
		when(courseDao.findById(1L)).thenThrow(NoResultException.class);
		
		// Calling findById method
		courseService.findById(1L);		
	}
	
	
	@Test
	public void deleteCourseTest_Successfull(){
		

		HashMap<String,String> result=new HashMap<String,String>();
		result.put("status", "ok");
		result.put("message","Course successfuly deleted");
		
		
		when(courseDao.deleteCourse(1L)).thenReturn(result);

		
		// Calling deleteStudent method
		courseService.deleteCourse(1L);
		
		// verifying times of calling deleteStudent() method
		verify(courseDao,times(1)).deleteCourse(1L);
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).deleteCourse(1L);
		
		
		//Assertion 
		assertEquals(result,courseDao.deleteCourse(1L));
		
	}
	
	@Test
	public void deleteCourseTest_NotSuccessfull(){
		

		HashMap<String,String> result=new HashMap<String,String>();
		
		  result.put("status", "err");
	      result.put("message","Course is not successfully deleted");

		when(courseDao.deleteCourse(1L)).thenReturn(result);

		
		// Calling deleteStudent method
		courseService.deleteCourse(1L);
		
		// verifying times of calling deleteStudent() method
		verify(courseDao,times(1)).deleteCourse(1L);
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).deleteCourse(1L);
		
		
		//Assertion 
		assertEquals(result,courseDao.deleteCourse(1L));
		
	}
	
	@Test
	public void addCourseTest_Successfull(){
		
		HashMap<String,String> result = new HashMap<String,String> ();
		
		result.put("status", "ok");
		result.put("message","Course successfuly added");
		
		// Course 1
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setContentOfProgram("Content of program for Calculus 1");
		course1.setFund_classes("3+2+1");
		course1.setGoalOfProgram("Goal of program for Calculus 1");
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");
		
		
		
		
		
		when(courseDao.addCourse(course1)).thenReturn(result);
		
		
		// Calling addCourse method
		courseService.addCourse(course1);
		
		// verifying times of calling addCourse() method
		verify(courseDao,times(1)).addCourse(course1);
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).addCourse(course1);
		
		
		//Assertion 
		assertEquals(result,courseDao.addCourse(course1));
		
	}
	
	@Test
	public void addCourseTest_NotSuccessfull(){
		
		HashMap<String,String> result = new HashMap<String,String> ();
		
		result.put("status", "err");
		result.put("message","Course is not successfully added");
		
		// Course 1
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setContentOfProgram("Content of program for Calculus 1");
		course1.setFund_classes(null);
		course1.setGoalOfProgram("Goal of program for Calculus 1");
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");

		when(courseDao.addCourse(course1)).thenReturn(result);
		
		
		// Calling addCourse method
		courseService.addCourse(course1);
		
		// verifying times of calling addCourse() method
		verify(courseDao,times(1)).addCourse(course1);
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).addCourse(course1);
		
		
		//Assertion 
		assertEquals(result,courseDao.addCourse(course1));
		
	}
	
	@Test
	public void updateCourseTest_Successfull(){
		HashMap<String,String> result = new HashMap<String,String> ();
				
		
		result.put("status", "ok");
		result.put("message","Course successfuly updated");
		 
		
		// Course 1
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setContentOfProgram("Content of program for Calculus 1");
		course1.setFund_classes("3+2+1");
		course1.setGoalOfProgram("GOAL");
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");
		
		
		
		
		
		when(courseDao.updateCourse(course1)).thenReturn(result);
		
		
		// Calling addCourse method
		courseService.updateCourse(course1);
		
		// verifying times of calling addCourse() method
		verify(courseDao,times(1)).updateCourse(course1);
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).updateCourse(course1);
		
		
		//Assertion 
		assertEquals(result,courseDao.updateCourse(course1));
		
	}
	@Test
	public void updateCourseTest_NotSuccessfull(){
		HashMap<String,String> result = new HashMap<String,String> ();
				
		
		result.put("status", "err");
        result.put("message","Course is not successfully updated");
		 
		
		// Course 1
		course1.setCourseId(1L);
		course1.setCourseName("Calculus 1");
		course1.setContentOfProgram("Content of program for Calculus 1");
		course1.setFund_classes("3+2+1");
		course1.setGoalOfProgram(null);
		course1.setLanguage("English");
		course1.setProfessor("Verica Bakeva");

		
		when(courseDao.updateCourse(course1)).thenReturn(result);
		
		
		// Calling addCourse method
		courseService.updateCourse(course1);
		
		// verifying times of calling addCourse() method
		verify(courseDao,times(1)).updateCourse(course1);
		
		// Order of executing
		InOrder order=inOrder(courseDao);
		order.verify(courseDao).updateCourse(course1);
		
		
		//Assertion 
		assertEquals(result,courseDao.updateCourse(course1));
		
	}
	@Test
	public void  addStudentDataTest_Successfull(){
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
				
				
				// Course 1
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setContentOfProgram("Content of program for Calculus 1");
				course1.setFund_classes("3+2+1");
				course1.setGoalOfProgram("Goal of program for Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
			// StudentCourse
				
				studentCourse1.setCourse(course1);
				studentCourse1.setStudent(student1);
				studentCourse1.setGrade(10.00);
				studentCourse1.setSchoolYear(2013);
				studentCourse1.setSemester("WINTER");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				

				result.put("status", "ok");
				result.put("message","Course successfuly added");
				
				when(courseDao.addStudentData(studentCourse1)).thenReturn(result);
				
				
				// Calling addCourse method
				courseService.addStudentData(studentCourse1);
				
				// verifying times of calling addCourse() method
				verify(courseDao,times(1)).addStudentData(studentCourse1);
				
				// Order of executing
				InOrder order=inOrder(courseDao);
				order.verify(courseDao).addStudentData(studentCourse1);
				
			
				//Assertion 
				assertEquals(result,courseDao.addStudentData(studentCourse1));
				
				
	
	}
	
	@Test
	public void  addStudentDataTest_NotSuccessfull(){
		
		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
				
				
			
				
			// StudentCourse
				
				studentCourse1.setCourse(course1);
				studentCourse1.setStudent(student1);
				studentCourse1.setGrade(10.00);
				studentCourse1.setSchoolYear(2013);
				studentCourse1.setSemester("WINTER");
				
				HashMap<String,String> result = new HashMap<String,String> ();
				

				result.put("status", "err");
				result.put("message","Student course  data is not successfuly added");
			
				
				when(courseDao.addStudentData(studentCourse1)).thenReturn(result);
				
				
				// Calling addCourse method
				courseService.addStudentData(studentCourse1);
				
				// verifying times of calling addCourse() method
				verify(courseDao,times(1)).addStudentData(studentCourse1);
				
				// Order of executing
				InOrder order=inOrder(courseDao);
				order.verify(courseDao).addStudentData(studentCourse1);
				
			
				//Assertion 
				assertEquals(result,courseDao.addStudentData(studentCourse1));
				
				
	
	}
	
	@Test
	public void getAllStudentCoursesTest_Successfull(){
		
		

		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
			
				// Course 1
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setContentOfProgram("Content of program for Calculus 1");
				course1.setFund_classes("3+2+1");
				course1.setGoalOfProgram("Goal of program for Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
				// Course 2
				course2.setCourseId(2L);
				course2.setCourseName("Calculus 2");
				course2.setContentOfProgram("Content of program for Calculus 2");
				course2.setFund_classes("3+2+1");
				course2.setGoalOfProgram("Goal of program for Calculus 2");
				course2.setLanguage("English");
				course2.setProfessor("Verica Bakeva");
				
			// StudentCourse 1
				
				studentCourse1.setCourse(course1);
				studentCourse1.setStudent(student1);
				studentCourse1.setGrade(10.00);
				studentCourse1.setSchoolYear(2013);
				studentCourse1.setSemester("WINTER");
				
			// StudentCourse 2	
				
				studentCourse2.setCourse(course2);
				studentCourse2.setStudent(student1);
				studentCourse2.setGrade(9.00);
				studentCourse2.setSchoolYear(2014);
				studentCourse2.setSemester("SUMMER");
				
				
				when(courseDao.getAllStudentCourses(1L)).thenReturn(Arrays.asList(studentCourse1,studentCourse2));
				
				
				// Calling addCourse method
				courseService.getAllStudentCourses(1L);
				
				// verifying times of calling addCourse() method
				verify(courseDao,times(1)).getAllStudentCourses(1L);
				
				// Order of executing
				InOrder order=inOrder(courseDao);
				order.verify(courseDao).getAllStudentCourses(1L);
				
			
				//Assertion 
				assertEquals(Arrays.asList(studentCourse1,studentCourse2),courseDao.getAllStudentCourses(1L));
				
		
	}
	
	
	@Test(expected=NoResultException.class)
	public void getAllStudentCoursesTest_NotSuccessfull_StudentNotFound(){
		
		

		student1.setId(1L);
		student1.setIndex(131000);
		student1.setYearOfEnrollment("2013");
		student1.setStatus(Status.REGULAR);
		student1.setCycle(StudiesCycle.FIRST);
		student1.setQuota(Quota.PRIVATE);
		student1.setStudiesProgram("KNI");
		student1.setPlaceOfLiving("Skopje");
		student1.setMunicipalityOfLiving("Karposh");
		student1.setCountry("Macedonia");
		student1.setAddress("Bihakjka");
		student1.setPhone("078000000");
		student1.setEmail("Mitre@yahoo.com");
		
		// Personal info
		student1.setName("Mitre");
		student1.setSurname("Mitrev");
		student1.setEMBG("123456987");
		student1.setDateOfBirth(new Date("01/02/1994"));
		student1.setNationality("Macedonian");
		student1.setGender(Gender.MALE);
		student1.setCitizenship("Macedonian");
		student1.setPlaceOfBirth("Skopje");
		student1.setMunicipalityOfBirth("Skopje");
		student1.setCountryOfBirth("Macedonia");
			
				// Course 1
				course1.setCourseId(1L);
				course1.setCourseName("Calculus 1");
				course1.setContentOfProgram("Content of program for Calculus 1");
				course1.setFund_classes("3+2+1");
				course1.setGoalOfProgram("Goal of program for Calculus 1");
				course1.setLanguage("English");
				course1.setProfessor("Verica Bakeva");
				
				// Course 2
				course2.setCourseId(2L);
				course2.setCourseName("Calculus 2");
				course2.setContentOfProgram("Content of program for Calculus 2");
				course2.setFund_classes("3+2+1");
				course2.setGoalOfProgram("Goal of program for Calculus 2");
				course2.setLanguage("English");
				course2.setProfessor("Verica Bakeva");
				
			// StudentCourse 1
				
				studentCourse1.setCourse(course1);
				studentCourse1.setStudent(student1);
				studentCourse1.setGrade(10.00);
				studentCourse1.setSchoolYear(2013);
				studentCourse1.setSemester("WINTER");
				
			// StudentCourse 2	
				
				studentCourse2.setCourse(course2);
				studentCourse2.setStudent(student1);
				studentCourse2.setGrade(9.00);
				studentCourse2.setSchoolYear(2014);
				studentCourse2.setSemester("SUMMER");
				
				
				when(courseDao.getAllStudentCourses(2L)).thenThrow(NoResultException.class);
				
				
				// Calling addCourse method
				courseService.getAllStudentCourses(2L);
				
				// verifying times of calling addCourse() method
				verify(courseDao,times(1)).getAllStudentCourses(2L);
				
				// Order of executing
				InOrder order=inOrder(courseDao);
				order.verify(courseDao).getAllStudentCourses(2L);
				
			
				
				
		
	}
	
	
}
